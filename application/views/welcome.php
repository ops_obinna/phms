<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FUT-Minna | Medical Center</title>
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/prettyPhoto.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/main.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/fontawesom.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>s
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>assets/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->
<body>
    <header class="navbar navbar-inverse navbar-fixed-top wet-asphalt" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><img src="<?php echo base_url(); ?>assets/images/futlogo.png" alt="logo"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="#">|</a></li>
                    <li><a href="<?php echo base_url(); ?>Login_Controller"><strong>LOGIN</strong></a></li><!--Link to Login -->
                </ul>
            </div>
        </div>
    </header><!--/header-->
    <section id="main-slider" class="no-margin">
        <div class="carousel slide wet-asphalt">
            <ol class="carousel-indicators">
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <li data-target="#main-slider" data-slide-to="1"></li>
                <li data-target="#main-slider" data-slide-to="2"></li>
                <li data-target="#main-slider" data-slide-to="3"></li>
                <li data-target="#main-slider" data-slide-to="4"></li>
                <li data-target="#main-slider" data-slide-to="5"></li>
                
            </ol>
            <div class="carousel-inner">
                <div class="item active" style="background-image: url(<?php echo base_url(); ?>assets/images/slider/phms5.png)">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content centered">
                                    <h1 class="animation animated-item-1" style="color:green";>Lifestyle based</h1>
                                    <h3 style="color:purple";><p class="animation animated-item-2"> The most important factors governing our health is how we think, eat and move in addition to the environment and friends you surround yourself with. Medical and therapeutic treatments should all be supported by a healthy lifestyle. All practitioners in the hospital/clinic should advocate and support healthy lifestyle changes.</p></h3>                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
                <div class="item" style="background-image: url(<?php echo base_url(); ?>assets/images/slider/phms.png)">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content center centered">
                                    <h2 class="boxed animation animated-item-1">Clean, Empathy, Humane and Responsive Health Care</h2>
                                    <p class="boxed animation animated-item-2">Our nurses are flexible, have physical endurance, empathy and interpersonal skill which is necessary for communication.</p>
                                    <br>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
                <div class="item" style="background-image: url(<?php echo base_url(); ?>assets/images/slider/phms2.png)">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content centered">
                                    <h2 class="animation animated-item-1">Integrated staff who cooperate, Healing Environment, Expertise and Competence </h2>
                                    <p class="animation animated-item-2"> It is our ideal to have a multi-disciplinary staff of many specialists from both the conventional and complementary health arenas who pool their knowledge together in designing an information and treatment protocol for each patient. The staff meet together, pool their findings and discuss the ideal program for each patient.</p>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs animation animated-item-4">
                           </div>
                        </div>
                    </div>
                </div><!--/.item-->
                 <div class="item" style="background-image: url(<?php echo base_url(); ?>assets/images/slider/phms3.png)">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content center centered">
                                    <h2 class="boxed animation animated-item-1">State of  the art integrated treatment and Affordable</h2>
                                    <p class="boxed animation animated-item-2">One of the greatest challenges is to make this type of individualized quality system of health care affordable. Fortunately FUT-Minna Clinic is breaking the JINX.</p>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
                 <div class="item" style="background-image: url(<?php echo base_url(); ?>assets/images/slider/phms4.png)">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content center centered">
                                    <h2 class="boxed animation animated-item-1">Thoughtful, Warm and Courteous Services</h2>
                                    <p class="boxed animation animated-item-2">In order to give back to the society, bring happiness to the people and offer seamless medical care services, FUT-Minna clinic is equipped with various advanced instruments, hardware and software. Our team of elites from different medical specialties offer thoughtful, friendly and sincere services to the public.</p>
                                    <br>
                                    <a class="btn btn-md animation animated-item-3" href="#">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
                <div class="item" style="background-image: url(<?php echo base_url(); ?>assets/images/slider/phms1.png)">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="carousel-content centered">
                                    <h1 class="animation animated-item-1" style="color:blue";>Dedication To Service of Humanity</h1>
                                    <h4><p class="boxed animation animated-item-2" style="color:white">Our resolve to the service of mankind have propelled us to an unquenchable test to save even more live in our communities</p></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="icon-angle-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="icon-angle-right"></i>
        </a>
    </section><!--/#main-slider-->

    <section id="services" class="emerald">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-user-md icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Our Team Doctors</h3>
                            <p>Our team of doctors have strong credentials are sincere and empathic with good reputation; have shown their professionalism in medical diagosis and treatment, be rest assured thet the best collection
                            	of medical doctors are always available to prescribe the right treatment</p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-3 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-medkit icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Pharmacist</h3>
                            <p>FUT-Clinic have always live to the expectation of keeping their pharmaceutical drugs up to date by offering the best drugs for the right prescription; providing the right medication for its staffs and students</p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-3 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-heart icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Our Nurses</h3>
                            <p>The Love, Passion, Care and Empathy of the Nurses is what give our patient hope to overcome challenges, The Nurses professionalism with a combination of humane keep us abreast</p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-3 col-sm-6">
                    <div class="media">
                        <div class="pull-left">
                            <i class="icon-stethoscope icon-md"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Laboratory Scientist</h3>
                            <p>Our Lab Scientist are one among the best laboratory scientist you can find anywhere, listening to detail and abiding by standards not only make us standout but also saves our patients lives</p>
                        </div>
                    </div>
                </div><!--/.col-md-4-->
            </div>
        </div>
    </section><!--/#services-->

    

   
   

    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2017 <a target="_blank" href="http://futinnovativeminds/" title="Federal University of Technology - Innovative Mindds Group">Federal University of Technology - Innovative Mindds Group</a>. All Rights Reserved.
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.prettyPhoto.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.js"></script>
</body>
</html>