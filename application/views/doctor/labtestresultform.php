<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Laboratory Test Result</li>
                        </ol>
                    </div>
                    
                </div>

                <br />

		<div class="row">
                    <!-- Column -->
                    <div class="col-sm-10">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Laboratory Request Form</h4>
                                
                                <hr>


                                    <!-- FORM STARTS HERE-->

        <?php $atts = array('id' => 'diagnosepatient', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
      <?php echo form_open('doctor/Dashboard/diagnosePatientDoc', $atts); ?>

        <!-- Flashdata if form submitted is success-->
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>

        <!-- Form data if form submitted is failure-->
        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
      
                <div><h3> Clinic details</h3></div>
                <hr>
                <div class="form-group">
                    <label>Patient ID</label>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_id" value = "" id="patient_id" autocomplete="off" readonly/>
                    </div>
                    
                </div>
                 <div class="form-group">
                    <label>Date</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        
                        <input type="text" format="Y-m-d" class="form-control" name="report_date" id="report_date" placeholder="Date" autocomplete="off" />
                    </div>
                    
                </div>

                <hr>
                <div><h3> Patient Details</h3></div>
                <hr>
                <div class="form-group">
                    
                    <div class="row">
                        <div class="col-sm">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="surname" placeholder = "surname"  autocomplete="off" readonly/>
                            </div>
                        </div>

                        <div class="col-sm">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="othername" placeholder = "other names"  autocomplete="off" readonly/>
                            </div>
                        </div>
                    </div>
                
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                        <label>Gender</label>
                        <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <select name="category" class="form-control" data-validate="required" name="gender" onchange="return get_class_sections(this.value)">
                              <option value="">Select Gender</option>
                              <option value="Male">Male</option>
                              <option value="Female">Female</option>
                             </select>
                        </div>
                      </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        
                        <input type="text" format="Y-m-d" class="form-control" name="date_prognosis" id="date_prognosis" placeholder="Date" autocomplete="off" />
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                            </div>
                            <input type="text" class="form-control" name="prognosis" placeholder = "Prognosis"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-8">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="specimen_type" placeholder = "specimen type"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            
                            <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" format="Y-m-d" class="form-control" name="specimen_date" id="specimen_date" placeholder="Date" autocomplete="off" />
                            
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="doctor_name" id="doctor_name" placeholder="doctor name" autocomplete="off" readonly />
                    </div>
                    </div>

                    <hr>
                    <div><h3>Haematology/Serology</h3></div>
                    <hr>
                    <div class="form-group">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">HB
                    <input type="checkbox" name="HB" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="HB (13 -18)" placeholder="HB (13 -18)"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="PVC (40-45)" placeholder="PVC (40-45)"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="WBC (4.8-10.8)" placeholder="WBC (4.8-10.8)"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                     <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="RBC (4.5-5.5)" placeholder="RBC (4.5-5.5)"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="MCV (76-93)" placeholder="MCV (76-93)"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="MCH (27-31)" placeholder="MCH (27-31)"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="MCHC (31-35)" placeholder="MCHC (31-35)"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="ESR (WESTER GREEN)" placeholder="ESR (WESTER GREEN)"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-8">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="DIFF.N,L,M,E,B" placeholder="DIFF. N,    L,    M,    E,    B"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="RETICS (0-2%)" placeholder="RETICS (0-2%)"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    
                    <div class="form-group">
                        <label>Blood film</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="BLOOD_FILM"  placeholder="BLOOD FILM" autocomplete="off"/>
                    </div>
                    </div>

                    <div class="form-group">
                    <div class="row">
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">HB GENOTYPE
                    <input type="checkbox" name="HB_GENOTYPE" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">SICKING
                    <input type="checkbox" name="SICKING" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">BLEED
                    <input type="checkbox" name="BLEED" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">CLOTT
                    <input type="checkbox" name="CLOTT" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">PLATELET COUNT
                    <input type="checkbox" name="PLATELET_COUNT" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                     <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">BLOODGROUP
                    <input type="checkbox" name="BLOODGROUP" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    </div>
                    </div>


                    <div class="form-group">
                    <div class="row">
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">VDRL TEST
                    <input type="checkbox" name="VDRL_TEST" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">TPHA
                    <input type="checkbox" name="TPHA" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">PREG TEST
                    <input type="checkbox" name="PREG_TEST" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">HIV
                    <input type="checkbox" name="HIV" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">HBS ag
                    <input type="checkbox" name="HBS_ag" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                     <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">HCV
                    <input type="checkbox" name="HCV" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    </div>
                    </div>

                    <hr>
                    <div><h3>Urine/Stool Analysis</h3></div>
                    <hr>
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-plus-square m-r-10"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="urine" id="urine" placeholder="URINE" autocomplete="off"/>
                    </div>
                    </div>
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-plus-square m-r-10"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="microscopy" id="microscopy" placeholder="MICROSCOPY" autocomplete="off"/>
                    </div>
                    </div>
                        <LABEL>Urine Chemistry</LABEL>
                        <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="P" placeholder="P"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="PROTEIN" placeholder="PROTEIN"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="BLOOD" placeholder="BLOOD"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="BILIYUBINE" placeholder="BILIYUBINE"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="UROBIINOGEN" placeholder="UROBIINOGEN"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="ASCOBIC_ACID" placeholder="ASCOBIC ACID"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="NITRATE" placeholder="NITRATE"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="SP_GRAVITY" placeholder="SP GRAVITY"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="EPITHELIAL_CELL" placeholder="EPITHELIAL CELL"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="RBC" placeholder="RBC" autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="YEAST_CELLS" placeholder="YEAST CELLS"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="CRYSTALS" placeholder="CRYSTALS"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="CASTS" placeholder="CASTS" autocomplete="off"/>
                            </div>
                        </div>

                    
                    </div>
                    
                    </div>
    
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="OVA" placeholder="OVA" autocomplete="off"/>
                    </div>
                    </div>

                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="TRICHOMONAS_VAGINALIS" placeholder="TRICHOMONAS VAGINALIS" autocomplete="off"/>
                    </div>
                    </div>

                   
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="OTHERS" placeholder="OTHERS" autocomplete="off"/>
                    </div>
                    </div>
                    <LABEL>WIDAL TEST</LABEL>
                    <div class="form-group">
                    <div class="row">                   
                    <div class="col-sm-4">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">SOMATIC ANTIGEN "O"
                    <input type="checkbox" name="SOMATIC_ANTIGEN_O" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                     <div class="col-sm-4">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">FLAGELLA ANTIGEN "H"
                    <input type="checkbox" name="FLAGELLA_ANTIGEN_H" value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    </div>
                    </div>

                         <div class="form-group">
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="PARATYPHI_A" placeholder="PARATYPHI  A:"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="PARATYPHIa" placeholder="PARATYPHI  a:"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="PARATYPHI_B" placeholder="PARATYPHI  B:"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="PARATYPHIb" placeholder="PARATYPHI  b:"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="PARATYPHI_C" placeholder="PARATYPHI  C:"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="PARATYPHIc" placeholder="PARATYPHI  c:"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="PARATYPHI_D" placeholder="PARATYPHID:"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" name="PARATYPHId" placeholder="PARATYPHI  d:"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">Summary</div>
                <div class="form-group">
                    <div class="input-group">
                        
                        <textarea rows="4" cols="130"  name="summary"  id="summary">
                        
                      </textarea>
                        
                    </div>
                
                </div>

                 
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="fa fa-login"></i>
                        Submit
                    </button>
                </div>             
    
            <?php echo form_close(); ?>
                                        
        <!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>


 <!--this script inignites the datepicker-->
     <script type="text/javascript">
        $(function(){
        $("#date_prognosis").datepicker({dateFormat: "yy-mm-dd" });
        $("#report_date").datepicker({dateFormat: "yy-mm-dd" });
        $("#specimen_date").datepicker({dateFormat: "yy-mm-dd" });
        });


    </script>