<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Microbiology Form</li>
                        </ol>
                    </div>
                    
                </div>

                <br />

		<div class="row">
                    <!-- Column -->
                    <div class="col-sm-8">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Microbiology Report Form</h4>
                                
                                <hr>


                                    <!-- FORM STARTS HERE-->

        <?php $atts = array('id' => 'diagnosepatient', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
      <?php echo form_open('', $atts); ?>

            <?php foreach ($view_patient as $patient) : ?>
            <div class="form-group">
                    
                    <div class="row">
                        <div class="col-sm">
                            <label>Patient ID</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="patient_id" value="<?php echo $patient->patient_id; ?>" id="patient_id" autocomplete="off" readonly/>
                            </div>
                        </div>
                    </div>
                
                </div>
                

                <div class="form-group">
                    
                    <div class="row">
                        <div class="col-sm">
                            <label>Surname</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="sname" value="<?php echo $patient->sname; ?>"  autocomplete="off" readonly/>
                            </div>
                        </div>

                        <div class="col-sm">
                            <label>Other names</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="oname" value="<?php echo $patient->oname; ?>"  autocomplete="off" readonly/>
                            </div>
                        </div>
                    </div>
                
                </div>

                <div class="form-group">
                    
                    <div class="row">
                        <div class="col-sm">
                            <label>Gender</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="gender" value="<?php echo $patient->gender; ?>"id="gender"  autocomplete="off" readonly />
                            </div>
                        </div>

                        <div class="col-sm">
                            <label>Date of Birth</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="dob"  value="<?php echo $patient->dob; ?>" id="dob" autocomplete="off" readonly />
                            </div>
                        </div>
                    </div>
                
                </div>

                <div class="form-group">
                    <label>Prognosis</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                         <textarea rows="4" cols="130"  name="prognosis"  id="prognosis"><?php echo $patient->prognosis; ?></textarea>
                        
                        
                    </div>
                    
                </div>

                <div class="form-group">
                    <label>Specimen / Investigation Required</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        <textarea rows="4" cols="130"  name="prognosis"  id="prognosis"><?php echo $patient->specimen_required; ?></textarea>
                    </div>
                    
                </div>

                <div class="form-group">
                    <div class="row">
                <div class="col-sm-6">
                    <label>Date of Investigation</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            
                            <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" format="Y-m-d" class="form-control" name="date_investigated" value="<?php echo $patient->date_investigated; ?>" id="date_investigated" placeholder="Date of Investigation" autocomplete="off" readonly />
                            
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label>Doctor ID</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text"  class="form-control" name="doc_id" id="doc_id" value="<?php echo $this->session->userdata('pfNo'); ?>" autocomplete="off" readonly/>
                            
                            </div>
                        </div>
                    </div>
                    </div>


                   
                    <hr>
                    <div><h2>REPORT:<em> For Lab Use</em></h2></div>
                    <hr>
                    <div class="form-group">
                    <label>Physical Characteristics</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        <textarea rows="3" cols="130"  name="phy_char" id="phy_char"><?php echo $patient->phy_char; ?></textarea>
                        
                    </div>
                    
                </div>


                <div class="form-group">
                    <label>Chemical Characteristics</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                        </div>
                        <textarea rows="3" cols="130"  name="chem_char" id="chem_char"><?php echo $patient->chem_char; ?></textarea>
                    </div>
                    
                </div>

                <div class="form-group">
                    <label>Microscope</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                        </div>
                        <textarea rows="3" cols="130"  name="mcroscope" id="microscope"><?php echo $patient->microscope; ?></textarea>
                    </div>
                    
                </div>

                <div class="form-group">
                    <label>Culture Yield</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                        </div>
                        <textarea rows="3" cols="130"  name="culture_yield" id="culture_yield"><?php echo $patient->culture_yield; ?></textarea>
                    </div>
                    
                </div>

                <div class="form-group">
                    <div class="row">
                 <div class="col-sm-6">
                            <label>Staff ID</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text"  class="form-control" name="staff_id" id="staff_id" autocomplete="off" readonly/>
                            
                            </div>
                        </div>
                    </div>
                    </div>

                    
                <div class="form-group">
                    <label>Summary</label>
                    <div class="input-group">
                        
                        <textarea rows="4" cols="130"  name="summary"  id="summary" readonly><?php echo $patient->summary; ?></textarea>
                        
                    </div>
                
                </div>

                 <?php endforeach; ?> 
                <div class="form-group">
                    <a href="<?php echo base_url(); ?>doctor/Dashboard/viewMicroBiologist" ><label>Back</label></a>
                </div>             
    
            <?php echo form_close(); ?>
                                        
        <!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>


 <!--this script inignites the datepicker-->
    <!-- <script type="text/javascript">
        $(function(){
        $("#dob_patientdetail").datepicker({dateFormat: "yy-mm-dd" });
        $("#date_investigated").datepicker({dateFormat: "yy-mm-dd" });
        });


    </script> -->