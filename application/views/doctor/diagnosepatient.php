<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Diagnose Patient</li>
                        </ol>
                    </div>
                    
                </div>

	<br />


	<div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Diagnosis Form</h4>
                                <hr>


		<!-- FORM STARTS HERE-->
		<?php $atts = array('id' => 'diagnosepatient', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
      <?php echo form_open('doctor/Dashboard/diagnosePatientDoc', $atts); ?>


        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>

        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
      
                <?php foreach ($patientdata as $patient) : ?>
                <div class="form-group">
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_name" id="patient_name" value = "<?php echo $patient->sname. " " . $patient->fname; ?>" placeholder="Patient Full Name" autocomplete="off" readonly/>
                    </div>
                    
                </div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_id" value = "<?php echo $patient->patient_id; ?>" id="patient_id" placeholder="Patient Identity" autocomplete="off" readonly/>
                    </div>
                    
                </div>


                	<div class="form-group">Previous Diagnosis</div>
                 <div class="form-group">
                    <div class="input-group">
                    	<textarea rows="4" cols="100"  name="prev_diagnosis" readonly><?php echo $patient->diagnosis;   ?></textarea>
                    </div>
                    
                </div>

                <div class="form-group">Diagnose Patient</div>
                <div class="form-group">
                    <div class="input-group">
                        
                        <textarea rows="4" cols="100"  name="diagnosis"  id="diagnosis"></textarea>
                        
                    </div>
                
                </div>

                 	


					

             <?php endforeach; ?>   	


					       
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="fa fa-login"></i>
                        Diagnose
                    </button>
                </div>


                <div class="btn-group">
                                                  
                    <?php echo anchor('doctor/Dashboard/previousRecord/' .$patient->patient_id.'','     Previous Health Record', 'class="fa fa-edit"'); ?>
                                     
                </div>

                
    
        
                
            <?php echo form_close(); ?>
										
		<!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>
