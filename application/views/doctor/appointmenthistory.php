<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Appointment History</li>
                        </ol>
                    </div>
                    
                </div>


                 <br />
                 <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title" style="color:blue;">ALL APPOINTMENTS</h4>
                                <div class="text-right">
                                    <h4 class="font-light m-b-0"><i class="fa fa-user-md" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted">This is the List of all Patients that  had appointment in the Clinic</span></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

		<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>
		
		<div class="card">
			
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="viewhistory">
			<thead  class="active">
				<tr>
					
					<th>ID</th>
					<th>Doctor Name</th>
					<td>Doctor PF Number</td>
					<th>Patient Name</th>
					<th>Patient ID</th>
					<th>Appointment Date and Time</th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_patient as $patient_list) : ?>
				<tr>
                    
					
					<td><?php echo $patient_list->appt_id; ?></td>
					<td><?php echo $patient_list->fname . " " . $patient_list->sname; ?></td>
					<td><?php echo $patient_list->pf_no; ?></td>
					<td><?php echo $patient_list->patient_name; ?></td>
					<td><?php echo $patient_list->patient_id; ?></td>
					<td><?php echo $patient_list->appt_date; ?></td>
					
				
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
	</div>
	


			<!-- Ignite Data Tables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#viewhistory").dataTable();
                	});
                </script>

