<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Patient Health History</li>
                        </ol>
                    </div>
                    
                </div>

                <br />

		<div class="row">
                    <!-- Column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Patient History View</h4>
                                
                                <hr>


                                    <!-- FORM STARTS HERE-->

        <?php $atts = array('id' => 'diagnosepatient', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
      <?php echo form_open(); ?>

      <?php foreach ($view_patient as $patient) : ?>
      
                <div><h3> Health History</h3></div>
                <hr>
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                        <label>Patient ID: <?php echo $patient->patient_id; ?></label>
                        <hr>
                        </div>

                        <div class="col-sm-6">
                        <label>Date Diagnosed: <?php echo $patient->diagnosis_date; ?></label>
                        <hr>
                        </div>
                </div>



                <div class="form-group">
                        <div class="row">

                        <div class="col-sm-6">
                            <label>Patient Name</label>
                            <div class="input-group">
                         <input type="text" class="form-control" value="<?php echo $patient->patient_name; ?>"  readonly/>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label>Sickness Diagnosed by Doctor</label>
                            <div class="input-group">
                            <textarea rows="4" cols="130"  name="summary"  id="summary">
                            <?php echo $patient->diagnosis; ?>
                            </textarea>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="form-group">
                        <div class="row">

                        <div class="col-sm-6">
                            <label>Diagnosed by</label>
                            <div class="input-group">
                         <input type="text" class="form-control" value="<?php echo "DR.". $patient->doc_name; ?>"  readonly/>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label>Doctor ID</label>
                            <div class="input-group">
                         <input type="text" class="form-control" value="<?php echo $patient->doc_id; ?>"  readonly/>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="form-group">
                        <div class="row">

                        <div class="col-sm-6">
                            <label>Doctor instruction to Nurse</label>
                            <div class="input-group">
                                <textarea rows="4" cols="130"  name="summary"  id="summary">
                            <?php echo $patient->nurse_prescription; ?>
                            </textarea>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <label>Medication update</label>
                            <div class="input-group">
                          <textarea rows="4" cols="130"  name="summary"  id="summary">
                            <?php echo $patient->injection_update; ?>
                            </textarea>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <label>Nurse</label>
                            <div class="input-group">
                         <input type="text" class="form-control" value="<?php echo $patient->nurse_id; ?>"  readonly/>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="form-group">
                        <div class="row">

                        <div class="col-sm-6">
                            <label>Doctor Instruction to Pharmacy</label>
                            <div class="input-group">
                                <textarea rows="4" cols="130"  name="summary"  id="summary">
                            <?php echo $patient->pharmacy_prescription; ?>
                            </textarea>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <label>Medication update</label>
                            <div class="input-group">
                          <textarea rows="4" cols="130"  name="summary"  id="summary">
                            <?php echo $patient->drug_update; ?>
                            </textarea>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <label>Pharmacist</label>
                            <div class="input-group">
                         <input type="text" class="form-control" value="<?php echo $patient->pharm_id; ?>"  readonly/>
                            </div>
                        </div>
                    </div> 
                </div>

                 
                <?php endforeach; ?> 
                 
                <div class="form-group">
                    <a href="<?php echo base_url(); ?>/doctor/Dashboard/medicalHistory"><label>Back</label></a>
                </div>             
    
            <?php echo form_close(); ?>
                                        
        <!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

