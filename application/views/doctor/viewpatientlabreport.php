<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Laboratory Test Result</li>
                        </ol>
                    </div>
                    
                </div>

                <br />

		<div class="row">
                    <!-- Column -->
                    <div class="col-sm-10">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Laboratory Request Form: <em style="color:blue;"> This is the Lab result for this patient</em></h4>
                                
                                <hr>


                                    <!-- FORM STARTS HERE-->

        <?php $atts = array('id' => 'diagnosepatient', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
      <?php echo form_open('doctor/Dashboard/diagnosePatientDoc', $atts); ?>

        <!-- Flashdata if form submitted is success-->
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>

        <!-- Form data if form submitted is failure-->
        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
      
                <div><h3> Clinic details</h3></div>
                <hr>
                <?php foreach ($patientdata as $patient) : ?>
                <div class="form-group">
                    <label>Patient ID</label>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_id" value = "<?php echo $patient->patient_id; ?>" id="patient_id" autocomplete="off" readonly/>
                    </div>
                    
                </div>
                 <div class="form-group">
                    <label>Report Date</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        
                        <input type="text" format="Y-m-d" value = "<?php echo $patient->report_date; ?>" class="form-control" name="report_date" id="report_date" placeholder="Date" autocomplete="off" />
                    </div>
                    
                </div>

                <hr>
                <div><h3> Patient Details</h3></div>
                <hr>
                <div class="form-group">
                    
                    <div class="row">
                        <div class="col-sm">
                            <label>Patient Surname</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->surname; ?>" name="surname" placeholder = "surname"  autocomplete="off" readonly/>
                            </div>
                        </div>

                        <div class="col-sm">
                            <label>Patient Othernames</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->othername; ?>" name="othername" placeholder = "other names"  autocomplete="off" readonly/>
                            </div>
                        </div>
                    </div>
                
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-4">
                        <label>Gender</label>
                        <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        <input type="text" class="form-control" value="<?php echo $patient->gender; ?>" name="gender" placeholder = "gender"  autocomplete="off" readonly/>
                        </div>
                      </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <label>Prognosis Date</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        
                        <input type="text"  value="<?php echo $patient->date_prognosis; ?>" format="Y-m-d" class="form-control" name="date_prognosis" id="date_prognosis" autocomplete="off" />
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <label>Prognosis</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                            </div>
                             <textarea rows="3" cols="130"  name="prognosis"><?php echo $patient->prognosis; ?></textarea>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-8">
                            <label>Specimen Type</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                        </div>
                        <textarea rows="3" cols="130"  name="specimen_type"><?php echo $patient->specimen_type; ?></textarea>
                        
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <label>Date</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            
                            <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" value="<?php echo $patient->specimen_date ?>" format="Y-m-d" class="form-control" name="specimen_date" id="specimen_date" autocomplete="off" />
                            
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <label>Doctor</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" value="<?php echo $patient->doctor_name; ?>" class="form-control" name="doctor_name" id="doctor_name" placeholder="doctor name" autocomplete="off" readonly />
                    </div>
                    </div>

                    <hr>
                    <div><h3>Haematology/Serology</h3></div>
                    <hr>
                    <div class="form-group">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">HB
                    <input type="checkbox" name="HB" <?php echo ($patient->HB=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->H_B; ?>" name="HB (13 -18)" placeholder="HB (13 -18)"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->PVC; ?>" name="PVC (40-45)" placeholder="PVC (40-45)"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->WBC; ?>" name="WBC (4.8-10.8)" placeholder="WBC (4.8-10.8)"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                     <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->RBCC; ?>" name="RBC (4.5-5.5)" placeholder="RBC (4.5-5.5)"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->MCV; ?>" name="MCV (76-93)" placeholder="MCV (76-93)"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->MCH; ?>" name="MCH (27-31)" placeholder="MCH (27-31)"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->MCHC; ?>" name="MCHC (31-35)" placeholder="MCHC (31-35)"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->ESR; ?>" name="ESR (WESTER GREEN)" placeholder="ESR (WESTER GREEN)"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-8">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->DIFF; ?>" name="DIFF.N,L,M,E,B" placeholder="DIFF. N,    L,    M,    E,    B"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->RETICS_PER; ?>" name="RETICS (0-2%)" placeholder="RETICS (0-2%)"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    
                    <div class="form-group">
                        <label>Blood film</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->BLOOD_FILM; ?>" name="BLOOD_FILM"  placeholder="BLOOD FILM" autocomplete="off"/>
                    </div>
                    </div>

                    <div class="form-group">
                    <div class="row">
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">HB GENOTYPE
                    <input type="checkbox" name="HB_GENOTYPE" <?php echo ($patient->HB_GENOTYPE=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">SICKING
                    <input type="checkbox" name="SICKING" <?php echo ($patient->SICKING=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">BLEED
                    <input type="checkbox" name="BLEED" <?php echo ($patient->BLEED=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">CLOTT
                    <input type="checkbox" name="CLOTT" <?php echo ($patient->CLOTT=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">PLATELET COUNT
                    <input type="checkbox" name="PLATELET_COUNT" <?php echo ($patient->PLATELET_COUNT=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                     <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">BLOODGROUP
                    <input type="checkbox" name="BLOODGROUP" <?php echo ($patient->BLOODGROUP=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    </div>
                    </div>


                    <div class="form-group">
                    <div class="row">
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">VDRL TEST
                    <input type="checkbox" name="VDRL_TEST" <?php echo ($patient->VDRL_TEST=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">TPHA
                    <input type="checkbox" name="TPHA" <?php echo ($patient->TPHA=='Y' ? 'checked' : '');?> value="POSITIVE" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">PREG TEST
                    <input type="checkbox" name="PREG_TEST" <?php echo ($patient->PREG_TEST=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">HIV
                    <input type="checkbox" name="HIV" <?php echo ($patient->HIV=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">HBS ag
                    <input type="checkbox" name="HBS_ag" <?php echo ($patient->HBS_ag=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                     <div class="col-sm-2">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">HCV
                    <input type="checkbox" name="HCV" <?php echo ($patient->HCV=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    </div>
                    </div>

                    <hr>
                    <div><h3>Urine/Stool Analysis</h3></div>
                    <hr>
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-plus-square m-r-10"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->urine; ?>" name="urine" id="urine" placeholder="URINE" autocomplete="off"/>
                    </div>
                    </div>
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-plus-square m-r-10"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->microscopy; ?>" name="microscopy" id="microscopy" placeholder="MICROSCOPY" autocomplete="off"/>
                    </div>
                    </div>
                        <LABEL>Urine Chemistry</LABEL>
                        <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->P; ?>" name="P" placeholder="P"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->PROTEIN; ?>" name="PROTEIN" placeholder="PROTEIN"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->BLOOD; ?>" name="BLOOD" placeholder="BLOOD"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->BILIYUBINE; ?>" name="BILIYUBINE" placeholder="BILIYUBINE"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->UROBIINOGEN; ?>" name="UROBIINOGEN" placeholder="UROBIINOGEN"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->ASCOBIC_ACID; ?>" name="ASCOBIC_ACID" placeholder="ASCOBIC ACID"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->NITRATE; ?>" name="NITRATE" placeholder="NITRATE"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->SP_GRAVITY; ?>" name="SP_GRAVITY" placeholder="SP GRAVITY"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->EPITHELIAL_CELL; ?>" name="EPITHELIAL_CELL" placeholder="EPITHELIAL CELL"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->RBC; ?>" name="RBC" placeholder="RBC" autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->YEAST_CELLS; ?>" name="YEAST_CELLS" placeholder="YEAST CELLS"  autocomplete="off"/>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->CRYSTALS; ?>" name="CRYSTALS" placeholder="CRYSTALS"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->CASTS; ?>" name="CASTS" placeholder="CASTS" autocomplete="off"/>
                            </div>
                        </div>

                    
                    </div>
                    
                    </div>
    
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->OVA; ?>" name="OVA" placeholder="OVA" autocomplete="off"/>
                    </div>
                    </div>

                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->TRICHOMONAS_VAGINALIS; ?>" name="TRICHOMONAS_VAGINALIS" placeholder="TRICHOMONAS VAGINALIS" autocomplete="off"/>
                    </div>
                    </div>

                   
                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->OTHERS; ?>" name="OTHERS" placeholder="OTHERS" autocomplete="off"/>
                    </div>
                    </div>
                    <LABEL>WIDAL TEST</LABEL>
                    <div class="form-group">
                    <div class="row">                   
                    <div class="col-sm-4">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">SOMATIC ANTIGEN "O"
                    <input type="checkbox" name="SOMATIC_ANTIGEN_O" <?php echo ($patient->SOMATIC_ANTIGEN_O=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                     <div class="col-sm-4">
                    <label class="checkboxlabel"style="display: block; position: relative;padding-left:25px;margin-bottom: 20px;cursor: pointer;  font-size: 14px;">FLAGELLA ANTIGEN "H"
                    <input type="checkbox" name="FLAGELLA_ANTIGEN_H" <?php echo ($patient->FLAGELLA_ANTIGEN_H=='Y' ? 'checked' : '');?> value="Y" style="height: 20px; width: 20px; position: absolute; top: 0; left: 0;">
                    <span class="checkmark"></span>
                    </label>
                    </div>
                    </div>
                    </div>

                         <div class="form-group">
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->PARATYPHI_A; ?>" name="PARATYPHI_A" placeholder="PARATYPHI  A:"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->PARATYPHIa; ?>" name="PARATYPHIa" placeholder="PARATYPHI  a:"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->PARATYPHI_B; ?>" name="PARATYPHI_B" placeholder="PARATYPHI  B:"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->PARATYPHIb; ?>" placeholder="PARATYPHI  b:"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->PARATYPHI_C; ?>" name="PARATYPHI_C" placeholder="PARATYPHI  C:"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" class="form-control" value="<?php echo $patient->PARATYPHIc; ?>" name="PARATYPHIc" placeholder="PARATYPHI  c:"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        
                        <input type="text" class="form-control" value="<?php echo $patient->PARATYPHI_D; ?>" name="PARATYPHI_D" placeholder="PARATYPHID:"  autocomplete="off"/>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                            </div>
                            <input type="text" value="<?php echo $patient->PARATYPHId; ?>" class="form-control" name="PARATYPHId" placeholder="PARATYPHI  d:"  autocomplete="off"/>
                            </div>
                        </div>
                    </div>
                    
                    </div>

                    <div class="form-group">Summary</div>
                <div class="form-group">
                    <div class="input-group">
                        
                        <textarea rows="4" cols="130"  name="summary"  id="summary"><?php echo $patient->summary; ?></textarea>
                        
                    </div>
                
                </div>

                <?php endforeach; ?>
                 
                <div class="form-group">
                    <button >
                        <a href="<?php echo base_url(); ?>doctor/Dashboard/viewGenLabReport"> Back to General Lab List</a>
                    </button>
                </div>             
    
            <?php echo form_close(); ?>
                                        
        <!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>


 <!--this script inignites the datepicker-->
    <!-- <script type="text/javascript">
        $(function(){
        $("#date_prognosis").datepicker({dateFormat: "yy-mm-dd" });
        $("#report_date").datepicker({dateFormat: "yy-mm-dd" });
        $("#specimen_date").datepicker({dateFormat: "yy-mm-dd" });
        });


    </script> -->