<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">General Laboratory Form</li>
                        </ol>
                    </div>
                    
                </div>

                <br />

		<div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Send to General Laboratory</h4>
                                
                                <hr>


                                    <!-- FORM STARTS HERE-->

        <?php $atts = array('id' => 'diagnosepatient', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
      <?php echo form_open('doctor/Dashboard/genLabRequest', $atts); ?>

        <!-- Flashdata if form submitted is success-->
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>

        <!-- Form data if form submitted is failure-->
        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
            <?php foreach ($patientdata as $patient) : ?>
                
                <div class="form-group">
                    <label>Patient ID</label>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_id" value = "<?php echo $patient->patient_id; ?>" id="patient_id" autocomplete="off" readonly/>
                    </div>
                    
                </div>
                 <div class="form-group">
                    <label>Patient Surname</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_sname" value = "<?php echo $patient->sname; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>
                   
                    <div class="form-group">
                        <label>Patient Othernames</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_fname" value = "<?php echo $patient->fname; ?>"  autocomplete="off" readonly/>
                    </div>
                    
                </div>
                 

                 <div class="form-group">
                        <label>Gender</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_gender" value = "<?php echo $patient->gender; ?>"  autocomplete="off" readonly/>
                    </div>
                    
                </div>

                <div class="form-group">
                        <label>Date of Birth</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-table"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="dob" value = "<?php echo $patient->dob; ?>"  autocomplete="off" readonly/>
                    </div>
                    
                </div>

                <div class="form-group">
                        <label>Type of Specimen</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-plus-square m-r-10"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="specimen_type"  autocomplete="off" />
                    </div>
                    
                </div>

                <div class="form-group">
                        <label>Prognosis</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-plus-square m-r-10"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="prognosis" value = ""  autocomplete="off"/>
                    </div>
                    
                </div>

                <div class="form-group">
                        <label>Investigation Required</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-plus-square m-r-10"></i>
                        </div>
                        
                        <textarea rows="3" cols="100"  name="required_invest"  id="required_invest"></textarea>
                    </div>
                    
                </div>

                
                <div class="form-group">
                    <label>Summary</label>
                    <div class="input-group">
                        
                        <textarea rows="4" cols="100"  name="summary"  id="summary"></textarea>
                        
                    </div>
                
                </div>

                 <?php endforeach; ?>       
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="fa fa-login"></i>
                        Send to General Laboratory
                    </button>
                </div>             
    
            <?php echo form_close(); ?>
                                        
        <!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>