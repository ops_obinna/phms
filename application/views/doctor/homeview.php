<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    
                </div>


                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">MY APPOINTMENTS</h4>
                                <div class="text-right">
                                    <h2 class="font-light m-b-0"><i class="fa fa-user-md" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted"><a href="<?php echo base_url(); ?>doctor/Dashboard/appointmentHistory" class="waves-effect">View Appointments History</a></span></h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">MANAGE PATIENTS</h4>
                                <div class="text-right">
                                    <h2 class="font-light m-b-0"><i class="fa fa-stethoscope" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted"><a href="<?php echo base_url(); ?>doctor/Dashboard/currentAppointment" class="waves-effect">Manage Patients</a></span></h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>