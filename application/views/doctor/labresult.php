<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Laboratory Result</li>
                        </ol>
                    </div>
                    
                </div>


                 <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">MICROBIOLOGIST</h4>
                                <div class="text-right">
                                    <h2 class="font-light m-b-0"><i class="fa fa-user-md" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted"><a href="<?php echo base_url(); ?>doctor/Dashboard/viewMicroBiologist" class="waves-effect">View Microbiologist Report</a></span></h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">GENERAL LAB REQUEST/REPORT</h4>
                                <div class="text-right">
                                    <h2 class="font-light m-b-0"><i class="fa fa-stethoscope" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted"><a href="<?php echo base_url(); ?>doctor/Dashboard/viewGenLabReport" class="waves-effect">View Gen. Lab. Request/Report form</a></span></h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
