<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Refer Patient</li>
                        </ol>
                    </div>
                    
                </div>

                 <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title" style="color:red;">NOTE</h4>
                                <div class="text-right">
                                    <h4 class="font-light m-b-0"><i class="fa fa-user-md" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted">Doctor can only Refer Patients they have Previously Diagnosed</a></span></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

	<br />


		
		<div class="card">
			
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="failedappointment" style="font-size:0.9em;">
			<thead  class="active">
				<tr>
					
					<th>ID</th>
					<th>Patient ID</th>
					<th>Patient Name</th>
					<th>Date Dignosed</th>			
					<th>Action</th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_patient as $patient_list) : ?>
				<tr>
					<td><?php echo $patient_list->appt_id; ?></td>
                	<td><?php echo $patient_list->patient_id; ?></td>
                	<td><?php echo $patient_list->patient_name; ?></td>
                	<td><?php echo $patient_list->appt_date; ?></td>
					
					<td>

						<div class="btn-group">
                           <button disabled>                       
                        <?php echo anchor('doctor/Dashboard/diagnosePatient/' .$patient_list->patient_id.'','     Diagnose', 'class="fa fa-edit"'); ?>
                             </button>        
                        </div>

					</td>
					
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
	</div>
	


			<!-- Ignite Data Tables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#failedappointment").dataTable();
                	});
                </script>

