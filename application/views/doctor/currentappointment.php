<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Current Appointments</li>
                        </ol>
                    </div>
                    
                </div>

			<div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">MY CURRENT APPOINTMENTS</h4>
                                <div class="text-right">
                                    <h2 class="font-light m-b-0"><i class="fa fa-user-md" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted">Doctor this are patients waiting to see you</a></span></h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title" style="color:red;">NOTE</h4>
                                <div class="text-left">
                                    <h5 class="font-light m-b-0"><i class="fa fa-user-md" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted">You Need to Confirm a patient before you diagnose; Also note once you confirm a patient, 
                                        the patient is removed from your view of current appointment, so be sure to click diagnose button and proceed with a patient diagnosis once the confirm button is clicked
                                        otherwise you loose track of patient and patient need to rebook appointment</a></span></h5>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	<br />

		<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>
		
		<div class="card">
		
		<?php if($view_patient) : ?>
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="currentHistory">
			<thead  class="active">
				<tr>
					
					<th>ID</th>
					<th>Doctor Name</th>
					<td>Doctor PF Number</td>
					<th>Patient Name</th>
					<th>Patient ID</th>
					<th>Appointment Date and Time</th>
					<th>Confrm</th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_patient as $patient_list) : ?>
				<tr>
                	<td><?php echo $patient_list->appt_id; ?></td>
					<td><?php echo $patient_list->fname . " " . $patient_list->sname; ?></td>
					<td><?php echo $patient_list->pf_no; ?></td>
					<td><?php echo $patient_list->patient_name; ?></td>
					<td><?php echo $patient_list->patient_id; ?></td>
					<td><?php echo $patient_list->appt_date; ?></td>
					
				<td>
					<div class="btn-group">
                                                  
                        <button>
                        <i class="fa fa-edit"> 
                        <a href="patientSeen/<?php echo $patient_list->appt_id; ?>" title="News title">Confirm Patient for Diagnosis</a>
                        </i>
                        </button>
                                  
                        </div>
				</td>
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>	
		<?php else : ?>


			<div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                
                                <div class="text-right">
                                    <h2 class="font-light m-b-0"><i class="fa fa-users" aria-hidden="true"></i>
                                     <?php echo "|"; ?><span class="text-muted">You Have No Appointments currently</a></span></h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	<?php endif; ?>
	
	</div>
	


			<!-- Ignite Data Tables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#currentHistory").dataTable();
                	});
                </script>

