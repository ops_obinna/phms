<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Previous Health Record</li>
                        </ol>
                    </div>
                    
                </div>

	<br />


	<div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Previous Health Record</h4>
                    
                                <hr>


									<!-- FORM STARTS HERE-->




		<?php $atts = array('id' => 'registerstaff', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
        <?php echo form_open('doctor/Dashboard/diagnosePatientDoc', $atts); ?>


        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>

        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
      
                <?php foreach ($patientdata as $patient) : ?>
                <div class="form-group">Doctor Name</div>
                <div class="form-group">
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="doctor_name" id="doctor_name" value = "<?php echo $patient->doctor_name; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>
                <div class="form-group">Patient Name</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_name"  id="patient_name" value = "<?php echo $patient->patient_name; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>

                <div class="form-group">Patient ID</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_id"  id="patient_id" value = "<?php echo $patient->patient_id; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>

                <div class="form-group">Doctor ID</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="doctor_id"  id="doctor_id" value = "<?php echo $patient->doctor_id; ?>" autocomplete="off" readonly/>
                    </div>
                </div>

                <div class="form-group">Previous Diagnosis</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="diagnosis"  id="diagnosis" value = "<?php echo $patient->diagnosis; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>


                <div class="form-group">Nurse Prescription</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="nurse_prescription"  id="nurse_prescription" value = "<?php echo $patient->nurse_prescription; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>


                <div class="form-group">Pharmacy Prescription</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="pharmacy_precription"  id="pharmacy_prescription" value = "<?php echo $patient->pharmacy_prescription; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>


                <div class="form-group">Laboratory Precription</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="lab_prescription"  id="diagnosis" value = "<?php echo $patient->lab_prescription; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>

                <div class="form-group">Prescription Date</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="prescription_date"  id="prescription_date" value = "<?php echo $patient->prescription_date; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>

                <div class="form-group">Test Result</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <textarea rows="4" cols="100"  name="test_result" id="test_result" readonly></textarea>
                    </div>
                    
                </div>

                <div class="form-group">Injection Update</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="injection_update"  id="injection_update" value = "<?php echo $patient->injection_update; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>

                <div class="form-group">Drug Update</div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="drug_update"  id="drug_update" value = "<?php echo $patient->drug_update; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>

             <?php endforeach; ?>   	


					       
                
               
                <div class="btn-group">
                                                  
                    <?php echo anchor('doctor/Dashboard/currentAppointment/' .'','     Back', 'class="fa fa-arrow"'); ?>
                                     
                </div>
        
                
            <?php echo form_close(); ?>
										
		<!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>
