<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Forward Patient to Other Units</li>
                        </ol>
                    </div>
                    
                </div>


                 <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title"style="color:red;">NOTE</h4>
                                <div class="text-right">
                                    <h4 class="font-light m-b-0"><i class="fa fa-user-md" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted">Click on the Forward Button to direct Patient to other Units</span></h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                  
                </div>

                <br />

		<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>
		<div class="card">
			
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="forwardpatient">
			<thead  class="active">
				<tr>
					
					
					<th>ID</th>
					<th>Patient ID</th>
					<th>Diagnosis</th>
					<th>Diagnosis Date</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($forwarddiagnosis as $diagnosisdata) : ?>
				<tr>
                    
					<td><?php echo $diagnosisdata->id; ?></td>
					<td><?php echo $diagnosisdata->patient_id; ?></td>
					<td><?php echo $diagnosisdata->diagnosis; ?></td>
					<td><?php echo $diagnosisdata->prescription_date; ?></td>
					<td>
						 <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Forward <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDIT CLASS REPS LINKS  -->
                                        <li>
                                            <button style="width: 90%; text-align: center;background-color: white;cursor: pointer;display: block;">
                                            	<?php echo anchor('doctor/Dashboard/nurseForm/' .$diagnosisdata->patient_id.'','     Send to Nurse', 'class="fa fa-edit"'); ?>
                                            </button>
                                                                                                  
                                        </li>

                          
                                        <li>
                                            <button style="width: 90%; text-align: center;background-color: white;cursor: pointer;display: block;">
                                            <?php echo anchor('doctor/Dashboard/pharmForm/' .$diagnosisdata->patient_id.'','    Send to Pharm','class="fa fa-edit"'); ?> 
                                             </button>   
                                                                                              
                                        </li>

                                        
                                        <li>
                                            <button style="width: 90%; text-align: center;background-color: white;cursor: pointer;display: block;">
                                            <?php echo anchor('doctor/Dashboard/labForm/' .$diagnosisdata->patient_id.'','    Gen Lab Form','class="fa fa-edit"'); ?> 
                                             </button>   
                                                                                              
                                        </li>
                                        
                                       <!-- <li>
                                            <button style="width: 90%; text-align: center;background-color: white;cursor: pointer;display: block;">
                                            <?php// echo anchor('doctor/Dashboard/labTestResultForm/' .$diagnosisdata->patient_id.'','   Lab Result','class="fa fa-edit"'); ?> 
                                             </button>   
                                                                                              
                                        </li> -->
                                        
                                        <li>
                                            <button style="width: 90%; text-align: center;background-color: white;cursor: pointer;display: block;">
                                            <?php echo anchor('doctor/Dashboard/microForm/' .$diagnosisdata->patient_id.'','    Microbiologist','class="fa fa-edit"'); ?> 
                                             </button>   
                                                                                              
                                        </li>
                                    </ul>
                                </div>
						</td>
                    
						
					
				
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
	</div>


			<!-- Ignite Data Tables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#forwardpatient").dataTable();
                	});
                </script>