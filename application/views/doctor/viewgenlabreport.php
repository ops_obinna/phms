<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">List of Patient Sent to General Laboratory</li>
                        </ol>
                    </div>
                    
                </div>

	<br />

	 <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title" style="color:red;">NOTE</h4>
                                <div class="text-left">
                                    <h4 class="font-light m-b-0"><i class="fa fa-user-md" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted">Doctors should note that only patients who their results are ready are displayed below</span></h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

		
		<div class="card">
			
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="currentHistory" style="font-size:0.9em;">
			<thead  class="active">
				<tr>
					
					<th>ID</th>
					<th>Patient ID</th>
					<th>Patient Name</th>
					<th>Date of Birth</th>
					<th>Submitted Date</th>
					<th>Doctor</th>
					<th>Action</th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_patient as $patient_list) : ?>
				<tr>
					<td><?php echo $patient_list->id; ?></td>
                	<td><?php echo $patient_list->patient_id; ?></td>
					<td><?php echo $patient_list->other_names . " " . $patient_list->surname; ?></td>
					<td><?php echo $patient_list->dob; ?></td>
					<td><?php echo $patient_list->submitted_date; ?></td>
					<td><?php echo $patient_list->doctor_id; ?></td>
					<td>

						<div class="btn-group">
                           <button disabled>                       
                        <?php echo anchor('doctor/Dashboard/viewGenLabReportDoctor/' .$patient_list->patient_id."/". $patient_list->submitted_date.'','     View', 'class="fa fa-plus-square m-r-10"'); ?>
                             </button>        
                        </div>

					</td>
					
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
	</div>
	


			<!-- Ignite Data Tables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#currentHistory").dataTable();
                	});
                </script>

