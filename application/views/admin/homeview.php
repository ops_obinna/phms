        <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                    
                </div>

                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">MANAGE STAFF</h4>
                                <div class="text-right">
                                    <h2 class="font-light m-b-0"><i class="fa fa-users" aria-hidden="true"></i>
                                       <a href="<?php echo base_url();?>admin/Dashboard/updateStaffRecord"> <?php echo "|"; ?><span class="text-muted">Total Staff : <?php echo $numOfStaff; ?></span></h2> </a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                