<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Update Staffs Record</li>
                        </ol>
                    </div>
                    
                </div>


<div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Staff Update form</h4>
                                <hr>


									<!-- FORM STARTS HERE-->

	    <?php $atts = array('id' => 'registerstaff', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
        <?php echo form_open('admin/Dashboard/updateStaff/'.$item->pfNo, $atts); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
      			
        	<div class="form-group">
                 	<?php $data = array(
							'name' 			=> 'id',
							'id' 			=> 'id',
							'placeholder'	=> 'id',
							'class' 		=> 'form-control',
							'value'			=>  $item->id

						) ;?>
                    <div class="input-group">
                        
                        
                        <?php echo form_hidden($data); ?>
                    </div>
                    
                </div>	
 
                <div class="form-group">

                	<?php $data = array(
							'name' 			=> 'pfNo',
							'id' 			=> 'pFNo',
							'placeholder'	=> 'Staff PF Number',
							'class' 		=> 'form-control',
							'value'			=>  $item->pfNo

						) ;?>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>
                 <div class="form-group">
                 	<?php $data = array(
							'name' 			=> 'fname',
							'id' 			=> 'fname',
							'placeholder'	=> 'First Name',
							'class' 		=> 'form-control',
							'value'			=>  $item->fname

						) ;?>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>


                 <div class="form-group">
                 	<?php $data = array(
							'name' 			=> 'sname',
							'id' 			=> 'sname',
							'placeholder'	=> 'Surname',
							'class' 		=> 'form-control',
							'value'			=>  $item->sname

						) ;?>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>

                 <div class="form-group">
                 	<?php $data = array(
							'name' 			=> 'email',
							'id' 			=> 'email',
							'placeholder'	=> 'Staff Email',
							'class' 		=> 'form-control',
							'value'			=>  $item->email

						) ;?>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>


                <!--<div class="form-group">

                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </div>
                        
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
                    </div>
                
                </div> -->

                 <div class="form-group">
					 <div class="input-group">
					 	<div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
							<select name="gender" class="form-control" data-validate="required" id="gender" onchange="return get_class_sections(this.value)">
                              <option value="<?php echo $item->gender; ?>"><?php echo $item->gender; ?></option>
                              <option value="Male">Male</option>

                              <option value="Female">Female</option>
                             </select>
						</div> 
					</div>


                    <!-- <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-home"></i>
                        </div>
                            <select name="state" class="form-control" data-validate="required" id="state" onchange="return get_class_sections(this.value)">
                              <option value="<?php //echo $item->state; ?>"><?php //echo $item->state; ?></option>
                              <?php 
                                $faculty_table //= $this->db->get('state')->result_array();
                                //foreach($faculty_table as $row):
                                    ?>
                                    <option value="<?php //echo $row['statename'];?>">
                                            <?php //echo $row['statename'];?>
                                    </option>
                                <?php
                                //endforeach;
                              ?>
                             </select>
                        </div> 
                    </div> -->

                     <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-home"></i>
                        </div>
                            <select name="state" class="form-control" data-validate="required" id="state" onchange="return get_class_sections(this.value)">
                              <option value="<?php echo $item->state ; ?>"><?php echo $item->statename; ?></option>
                              <?php 
                                
                                foreach($state as $row):
                                    ?>
                                    <option value="<?php echo $row['id']; ?>" >
                                           <?php echo $row['statename']; ?>
                                    </option>
                                <?php
                                endforeach;
                              ?>
                             </select>
                        </div> 
                    </div>


                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-home"></i>
                        </div>
                            <select name="city" class="form-control" data-validate="required" id="city" onchange="return get_class_sections(this.value)">
                              <option value="<?php echo $item->city; ?>"><?php echo $item->lganame; ?></option>
                              
                             </select>
                        </div> 
                    </div>


                	

					<div class="form-group">
						<?php $data = array(
							'name' 			=> 'phone',
							'id' 			=> 'phone',
							'placeholder'	=> 'Mobile Phone Contact',
							'class' 		=> 'form-control',
							'value'			=>  $item->phone

						) ;?>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                	</div>


                	<div class="form-group">
                    <div class="input-group">
					 	<div class="input-group-addon">
                            <i class="fa fa-archive"></i>
                        </div>
							<select name="specialization" class="form-control" data-validate="required" id="specialization" onchange="return get_class_sections(this.value)">
                              <option value="<?php echo $item->specialization; ?>"><?php echo $item->specialization; ?></option>
                              <option value="Doctor">Doctor</option>
                              <option value="Nurse">Nurse</option>
                              <option value="Laboratory Scientist">Laboratory Scientist</option>
                              <option value="Record">Record Officer</option>
                             </select>
						</div> 
					</div>


					<div class="form-group">
						<?php $data = array(
							'name' 			=> 'doctorspecialization',
							'id' 			=> 'doctorspecialization',
							'placeholder'	=> 'Doctor Specialization',
							'class' 		=> 'form-control',
							'value'			=>  $item->doctorspecialization

						) ;?>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user-md"></i>
                        </div>		
                        
                        <?php echo form_input($data); ?>
                    </div>
                	</div>


                    

                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="fa fa-login"></i>
                        Submit Staff Record
                    </button>
                </div>
    
        
                
            <?php echo form_close(); ?>
										
		<!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>