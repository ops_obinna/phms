<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Create Staff </li>
                        </ol>
                    </div>
                    
                </div>


                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Registration form</h4>
                                <hr>


									<!-- FORM STARTS HERE-->




		<?php $atts = array('id' => 'registerstaff', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
      <?php echo form_open('admin/Dashboard/registerStaff', $atts); ?>


        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>

        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
      
                
                <div class="form-group">
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="pfNo" id="pfNo" placeholder="Staff PF Number" autocomplete="off" />
                    </div>
                    
                </div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" autocomplete="off" />
                    </div>
                    
                </div>


                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="sname" id="sname" placeholder="Surname" autocomplete="off" />
                    </div>
                    
                </div>

                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="email" id="email" placeholder="Staff Email" autocomplete="off" />
                    </div>
                    
                </div>


                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </div>
                        
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off" />
                    </div>
                
                </div>

                 <div class="form-group">
					 <div class="input-group">
					 	<div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
							<select name="gender" class="form-control" data-validate="required" id="gender" onchange="return get_class_sections(this.value)">
                              <option value="">Gender</option>
                              <option value="Male">Male</option>

                              <option value="Female">Female</option>
                             </select>
						</div> 
					</div>

                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-home"></i>
                        </div>
                            <select name="state" class="form-control" data-validate="required" id="state" onchange="return get_class_sections(this.value)">
                              <option value="">State</option>
                              <?php 
                                
                                foreach($state as $row):
                                    ?>
                                    <option value="<?php echo $row['id']; ?>" >
                                            <?php echo $row['statename']; ?>
                                    </option>
                                <?php
                                endforeach;
                              ?>
                             </select>
                        </div> 
                    </div>

                	<div class="form-group">
                    <div class="input-group">
					 	<div class="input-group-addon">
                            <i class="fa fa-home"></i>
                        </div>
							<select name="city" class="form-control" data-validate="required" id="city" onchange="return get_class_sections(this.value)">
                              <option value="">City</option>
                              
                             </select>
						</div> 
					</div>


					<div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        
                        <input type="phone" class="form-control" name="phone" id="city" placeholder="Mobile Phone Contact" autocomplete="off" />
                    </div>
                	</div>

                	<div class="form-group">
                    <div class="input-group">
					 	<div class="input-group-addon">
                            <i class="fa fa-archive"></i>
                        </div>
							<select name="specialization" class="form-control" data-validate="required" id="specialization" onchange="return get_class_sections(this.value)">
                              <option value="">Choose Staff Specialization</option>
                              <option value="Doctor">Doctor</option>
                              <option value="Nurse">Nurse</option>
                              <option value="Laboratory Scientist">Laboratory Scientist</option>
                              <option value="Record">Record Officer</option>
                              <option value="Pharmacist">Pharmacist</option>
                             </select>
						</div> 
					</div>


					<div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user-md"></i>
                        </div>		
                        
                        <input type="doctorspecialization" class="form-control" name="doctorspecialization" id="doctorspecialization" placeholder="Doctor Specialization" autocomplete="off" />
                    </div>
                	</div>            
                
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="fa fa-login"></i>
                        Submit Staff Record
                    </button>
                </div>
    
        
                
            <?php echo form_close(); ?>
										
		<!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>