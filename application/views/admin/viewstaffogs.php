<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">View Staff Records</li>
                        </ol>
                    </div>
                    
                </div>


                <div class="container">
                	<table class="table table-bordered table-hover table-striped" id="logs">
                		<thead>
                			<tr>
                				<th>ACTIVITY ID</th>
                				<th>STAFF PF NUMBER</th>
                				<th>EMAIL ACCOUNT</th>
                				<th>TIME OF TASK</th>
                				<th>TYPE OF ACTIVITY</th>
                			</tr>	
                		</thead>	
                		<tbody>
                			<?php foreach ($userlog as $stafflog) : ?>
                			<tr>
                			<td><?php echo $stafflog->id; ?></td>
							<td><?php echo $stafflog->pfNo; ?></td>
							<td><?php echo $stafflog->email; ?></td>
							<td><?php echo $stafflog->task_time; ?></td>
							<td><?php echo $stafflog->task; ?></td>
						</tr>

               			<?php endforeach; ?>

                		</tbody>
                	</table>

                </div>

                <script type="text/javascript">
                	$(function(){

                		$("#logs").dataTable();
                	});
                </script>





                