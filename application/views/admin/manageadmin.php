<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Update Admin Password</li>
                        </ol>
                    </div>
                    
                </div>

                <div class="row">
                    <!-- Column -->
                    <div class="col-md-10">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Change your password</h4>
                                <hr>


                 <?php 
                    
                        $multi = array(
                            'class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'
                                    );
                        ?>

				        <?php echo form_open(base_url() . '/admin/Dashboard/updateAdminPassword', $multi); ?> 
						
						
						
							<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-5"><?php echo validation_errors('<p class="alert alert-danger">'); ?></div>
							<div class="col-md-4"></div>
							</div>



							<div class="form-group">
		                    <div class="input-group">
		                        <div class="input-group-addon">
		                            <i class="fa fa-key"></i>
		                        </div>
		                        
		                        <input type="password" class="form-control" name="password" id="password" placeholder="Enter Current Password" autocomplete="off" />
		                    </div>
		                
		                	</div>

		                	<div class="form-group">
		                    <div class="input-group">
		                        <div class="input-group-addon">
		                            <i class="fa fa-key"></i>
		                        </div>
		                        
		                        <input type="password" class="form-control" name="new_password" id="password" placeholder="Enter Your New  Password" autocomplete="off" />
		                    </div>
		                
		                	</div>

		                	<div class="form-group">
		                    <div class="input-group">
		                        <div class="input-group-addon">
		                            <i class="fa fa-key"></i>
		                        </div>
		                        
		                        <input type="password" class="form-control" name="confirm_new_password" id="password" placeholder="Comfirm your New Password" autocomplete="off" />
		                    </div>
		                
		                	</div>
                            
                  
                            <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-5">
                                  <button type="submit" class="btn btn-info"><?php echo('Update Password');?></button>
                              </div>
								</div>
                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>