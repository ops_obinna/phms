<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">View Staffs Record</li>
                        </ol>
                    </div>
                    
                </div>




		<br />

		<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>

		<div class="card">
			
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="table-4">
			<thead  class="active">
				<tr>
					
					<th>PFNO</th>
					<th>First Name</th>
					<th>Surname</th>
					<th>Email</th>
					<th>Gender</th>
					<th>City</th>
					<th>State</th>
					<th>DOR</th>				
					<th>Phone</th>
					<th>Specialization</th>
					<th>Specialty</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_staff as $staff_list) : ?>
<!--<img src="<?//php echo base_url().'upload/classreps/'.$reps_list->rep_img; ?>" class="img-circle" width="30" >-->
				<tr>
                    
					
					<td><?php echo $staff_list->pfNo; ?></td>
					<td><?php echo $staff_list->fname; ?></td>
					<td><?php echo $staff_list->sname; ?></td>
					<td><?php echo $staff_list->email; ?></td>
					<td><?php echo $staff_list->gender; ?></td>
					<td><?php echo $staff_list->lganame; ?></td>
					<td><?php echo $staff_list->statename; ?></td>
					<td><?php echo $staff_list->regdate; ?></td>
					<td><?php echo $staff_list->phone; ?></td>
					<td><?php echo $staff_list->specialization; ?></td>
					<td><?php echo $staff_list->doctorspecialization; ?></td>
					<td>
						 <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDIT CLASS REPS LINKS  -->
                                        <li>
                                            
                                            
                                            <?php echo anchor('admin/Dashboard/editStaff/' .$staff_list->pfNo.'','     Edit', 'class="fa fa-edit"'); ?>
                                                                                                  
                                        </li>
                                        <!-- DELETE PROFILE LINK -->
                                        <li>
                                            
                                            <?php echo anchor('admin/Dashboard/deleteStaff/' .$staff_list->pfNo.'','    Delete','class="fa fa-trash"'); ?> 
                                                
                                                                                              
                                        </li>
                                        
                                    </ul>
                                </div>
						</td>
                    
						
					
				
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
	</div>
	
		
		








                