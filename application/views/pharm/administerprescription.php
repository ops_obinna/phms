<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Administer Medication</li>
                        </ol>
                    </div>
                    
                </div>

                <br />

        <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Medication</h4>
                                
                                <hr>


                                    <!-- FORM STARTS HERE-->

        <?php $atts = array('id' => 'diagnosepatient', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
      <?php echo form_open('pharm/Dashboard/pharmFunction', $atts); ?>

        <!-- Flashdata if form submitted is success-->
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>

        <!-- Form data if form submitted is failure-->
        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
      
                <?php foreach ($viewpatient as $patient) : ?>
                <div class="form-group">
                    <label>Patient ID</label>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_id" value = "<?php echo $patient->patient_id; ?>" id="patient_id" autocomplete="off" readonly/>
                    </div>
                    
                </div>
                 <div class="form-group">
                    <label>Patient Diagnosis</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
            <input type="text" class="form-control" name="diagnosis" value = "<?php echo $patient->diagnosis; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>


                    
                    <div class="form-group">
                        <label>Diagnosed Date</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-table"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="prescription_date"  value="<?php echo $patient->prescription_date; ?>" autocomplete="off" readonly/>
                    </div>
                    
                </div>
                 
                
                <div class="form-group">
                    <label>Medication Administered</label>
                    <div class="input-group">
                        
                        <textarea rows="4" cols="100"  name="pharmacy_prescription"  id="pharmacy_prescription"></textarea>
                        
                    </div>
                
                </div>

                 <?php endforeach; ?> 
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="fa fa-login"></i>
                        Administer
                    </button>
                </div>             
    
            <?php echo form_close(); ?>
                                        
        <!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>