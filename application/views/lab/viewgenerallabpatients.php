<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">List of Patient for General Laboratory Test</li>
                        </ol>
                    </div>
                    
                </div>

                 <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title" style="color:red;">NOTE</h4>
                                <div class="text-right">
                                    <h2 class="font-light m-b-0"><i class="fa fa-user-md" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted">This are Patients not yet Examined</span></h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>

	<br />

		

        <!-- Form data if form submitted is failure-->
        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
		
		<div class="card">
			<?php if($view_patient) : ?>
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="currentHistory" style="font-size:0.9em;">
			<thead  class="active">
				<tr>
					
					<th>ID</th>
					<th>Patient ID</th>
					<th>Patient Name</th>
					<th>Doctor ID</th>
					<th>Submitted Date</th>
					<th>Action</th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_patient as $patient_list) : ?>
				<tr>
					<td><?php echo $patient_list->id; ?></td>
                	<td><?php echo $patient_list->patient_id; ?></td>
					<td><?php echo $patient_list->other_names. "  ". $patient_list->surname ; ?></td>
					<td><?php echo $patient_list->doctor_id; ?></td>
					<td><?php echo $patient_list->submitted_date; ?></td>
					
					<td>

						<div class="btn-group">
                           <button disabled>                       
                        <?php echo anchor('lab/Dashboard/labRequest/' .$patient_list->patient_id.'','     View Request', 'class="fa fa-medkit"'); ?>
                             </button>        
                        </div>

					</td>
					
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
		<?php else : ?>
	<div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <div class="text-right">
                                    <h2 class="font-light m-b-0"><i class="fa fa-users" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted">No patient waiting to be examined</span></h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
	<?php endif; ?>
	
	</div>
	


			<!-- Ignite Data Tables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#currentHistory").dataTable();
                	});
                </script>

