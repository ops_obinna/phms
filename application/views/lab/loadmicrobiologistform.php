<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Microbiology Form: Report back to Doctor</li>
                        </ol>
                    </div>
                    
                </div>

                <br />

		<div class="row">
                    <!-- Column -->
                    <div class="col-sm-8">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Microbiology Form:<em> Report Form</em></h4>
                                
                                <hr>


                                    <!-- FORM STARTS HERE-->

        <?php $atts = array('id' => 'diagnosepatient', 'class' => 'form-registerstaff', 'role' => 'form'); ?>
      <?php echo form_open('lab/Dashboard/microFunction', $atts); ?>

        <!-- Flashdata if form submitted is success-->
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>

        <!-- Form data if form submitted is failure-->
        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>

            <?php foreach ($patientdata as $patient) : ?>
            <div class="form-group">
                    
                    <div class="row">
                        <div class="col-sm">
                            <label>Patient ID</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="patient_id" value="<?php echo $patient->patient_id; ?>" id="patient_id" autocomplete="off" readonly/>
                            </div>
                        </div>
                    </div>
                
                </div>
                

                <div class="form-group">
                    
                    <div class="row">
                        <div class="col-sm">
                            <label>Surname</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="sname" value="<?php echo $patient->sname; ?>"  autocomplete="off" readonly/>
                            </div>
                        </div>

                        <div class="col-sm">
                            <label>Other names</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="oname" value="<?php echo $patient->oname; ?>"  autocomplete="off" readonly/>
                            </div>
                        </div>
                    </div>
                
                </div>

                <div class="form-group">
                    
                    <div class="row">
                        <div class="col-sm">
                            <label>Gender</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="gender" value="<?php echo $patient->gender; ?>"id="gender"  autocomplete="off" readonly />
                            </div>
                        </div>

                        <div class="col-sm">
                            <label>Date of Birth</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text" class="form-control" name="dob"  value="<?php echo $patient->dob; ?>" id="dob" autocomplete="off" readonly />
                            </div>
                        </div>
                    </div>
                
                </div>

                <div class="form-group">
                    <label>Prognosis</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        <textarea rows="3" cols="130"  name="prognosis" id="prognosis"><?php echo $patient->prognosis ?></textarea>
                        
    
                    </div>
                    
                </div>

                <div class="form-group">
                    <label>Specimen / Investigation Required</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-pencil"></i>
                        </div>
                        <textarea rows="3" cols="130"  name="specimen_required" id="specimen_required"><?php echo $patient->specimen_required; ?></textarea>
                        
                    </div>
                    
                </div>

                <div class="form-group">
                    <div class="row">
                <div class="col-sm-6">
                    <label>Date of Investigation</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            
                            <i class="fa fa-clock-o"></i>
                            </div>
                            <input type="text" format="Y-m-d" class="form-control" value="<?php echo $patient->date_investigated ?>" name="date_investigated" id="date_investigated" placeholder="Date of Investigation" autocomplete="off" readonly/>
                            
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label>Doctor ID</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text"  class="form-control" name="doc_id" id="doc_id" value="<?php echo $this->session->userdata('pfNo'); ?>" autocomplete="off" readonly/>
                            
                            </div>
                        </div>
                    </div>
                    </div>


                   
                    <hr>
                    <div><h2>REPORT:<em> For Lab Use</em></h2></div>
                    <hr>
                    <div class="form-group">
                    <label>Physical Characteristics</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        <textarea rows="3" cols="130"  class="form-control" name="phy_char" id="phy_char"></textarea>
                        
                    </div>
                    
                </div>


                <div class="form-group">
                    <label>Chemical Characteristics</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                        </div>
                        <textarea rows="3" cols="130"  class="form-control" name="chem_char" id="chem_char"></textarea>
                        
                        
                    </div>
                    
                </div>

                <div class="form-group">
                    <label>Microscope</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                        </div>
                        <textarea rows="3" cols="130"  class="form-control" name="microscope" id="microscope"></textarea>
                    </div>
                    
                </div>

                <div class="form-group">
                    <label>Culture Yield</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-medkit"></i>
                        </div>

                        <textarea rows="3" cols="130" class="form-control" name="culture_yield" id="culture_yield"></textarea>
                        
                    </div>
                    
                </div>

                <div class="form-group">
                    <div class="row">
                 <div class="col-sm-6">
                            <label>Staff ID</label>
                            <div class="input-group">
                            <div class="input-group-addon">
                            
                            <i class="fa fa-user"></i>
                            </div>
                            <input type="text"  class="form-control" name="staff_id" value="<?php echo $this->session->userdata('pfNo'); ?>" id="staff_id" placeholder="staff id" autocomplete="off" readonly/>
                            
                            </div>
                        </div>
                    </div>
                    </div>

                    
                <div class="form-group">
                    <label>Summary</label>
                    <div class="input-group">
                        
                        <textarea rows="4" cols="130" class="form-control"  name="summary"  id="summary"></textarea>
                        
                    </div>
                
                </div>

                 <?php endforeach; ?> 
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="fa fa-login"></i>
                        Submit
                    </button>
                </div>             
    
            <?php echo form_close(); ?>
                                        
        <!--FORM ENDS HERE -->
                         </div>
                        </div>
                    </div>
                    
                </div>


 