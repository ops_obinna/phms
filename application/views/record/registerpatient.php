
<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Register Patient</li>
                        </ol>
                    </div>
                    
                </div>




                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Registration form</h4>
                                <hr>


									<!-- FORM STARTS HERE-->




		<?php $atts = array('id' => 'submitPatientRecord', 'class' => 'form-submitPatientRecord', 'role' => 'form'); ?>
        <?php echo form_open('record/Dashboard/submitPatientRecord', $atts); ?>

        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
      
                
                <div class="form-group">
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="sname" id="sname" placeholder="Patient Surname" autocomplete="off" />
                    </div>
                    
                </div>
                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="fname" id="fname" placeholder="Patient First Name" autocomplete="off" />
                    </div>
                    
                </div>


                 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        
                        <input type="text" format="Y-m-d" class="form-control" name="dob" id="dob" placeholder="Date of Birth" autocomplete="off" />
                    </div>
                    
                </div>

                 <div class="form-group">
					 <div class="input-group">
					 	<div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
							<select name="gender" class="form-control" data-validate="required" id="gender" onchange="return get_class_sections(this.value)">
                              <option value="">Gender</option>
                              <option value="Male">Male</option>

                              <option value="Female">Female</option>
                             </select>
						</div> 
					</div>

					<div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="phone" id="phone" placeholder="Mobile Phone Contact" autocomplete="off" />
                    </div>
                	</div>


                	<div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="email" id="email" placeholder="Patient Email Address" autocomplete="off" />
                    </div>
                	</div>

                	<div class="form-group">
					 <div class="input-group">
					 	<div class="input-group-addon">
                            <i class="fa fa-question-circle"></i>
                        </div>
							<select name="category" class="form-control" data-validate="required" id="category" onchange="return get_class_sections(this.value)">
                              <option value="">Choose Category</option>
                              <option value="Student">Student</option>
                              <option value="Staff">Staff</option>
                              <option value="Others">Others</option>
                             </select>
						</div> 
					</div>


					<div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="patient_id" id="patient_id" placeholder="Patient ID" autocomplete="off" />
                    </div>
                	</div>

                	 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-home"></i>
                        </div>
                            <select name="state" class="form-control" data-validate="required" id="state" onchange="return get_class_sections(this.value)">
                              <option value="">State</option>
                              <?php 
                                
                                foreach($state as $row):
                                    ?>
                                    <option value="<?php echo $row['id']; ?>" >
                                            <?php echo $row['statename']; ?>
                                    </option>
                                <?php
                                endforeach;
                              ?>
                             </select>
                        </div> 
                    </div>


                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-home"></i>
                        </div>
                            <select name="city" class="form-control" data-validate="required" id="city" onchange="return get_class_sections(this.value)">
                              <option value="">City</option>
                              
                             </select>
                        </div> 
                    </div>

					
					<div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>		
                        
                        <input type="text" class="form-control" name="nok_fullname" id="nok_fullname" placeholder="Next of Kin Fullname" autocomplete="off" />
                    </div>
                	</div>


                	<div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        
                        <input type="text" class="form-control" name="nok_phone" id="nok_phone" placeholder="Next of Kin Phone Contact" autocomplete="off" />
                    </div>
                	</div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="fa fa-login"></i>
                        Submit Patient Record
                    </button>
                </div>
                    
            <?php echo form_close(); ?>
										
		<!--FORM ENDS HERE -->
        </div>
       </div>
      </div>


       <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Note </h4>
                                <div class="text-left">
                                    
                                    <span class="text-muted">Record officer should note that if the patient belongs to others
                                                             category then there is no need to enter any patient ID the system automatically 
                                                             creates a patient ID for non students/non staffs; but for student the
                                                             patient ID is the student ID and for staff the staff PF Number is the Patient ID.  
                                    </span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
     </div>

     <!--this script inignites the datepicker-->
     <script type="text/javascript">
        $(function() {
        $("#dob").datepicker({ dateFormat: "yy-mm-dd" });
        });
    </script>