<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Book Patient</li>
                        </ol>
                    </div>
                    
                </div>


<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>
<div class = "row">
		<div class="card">
			
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="bookpat">
			<thead  class="active">
				<tr>
					
					
					<th>First Name</th>
					<th>Surname</th>
					<th>Age</th>
					<th>Category</th>
					<th>Patient ID</th>
					<th>Gender</th>
					<th>Phone</th>				
					<th>Next of Kin</th>
					<th>Next of Kin Contact</th>
					<th>Registered Date</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($apptdata as $apptdata_list) : ?>
				<tr>
                    
					
					
					<td><?php echo $apptdata_list->fname; ?></td>
					<td><?php echo $apptdata_list->sname ?>
					<td><?php echo $apptdata_list->age; ?></td>
					<td><?php echo $apptdata_list->category; ?></td>
					<td><?php echo $apptdata_list->patient_id; ?></td>
					<td><?php echo $apptdata_list->gender; ?></td>
					<td><?php echo $apptdata_list->phone; ?></td>
					<td><?php echo $apptdata_list->nok_phone; ?></td>
					<td><?php echo $apptdata_list->nok_fullname; ?></td>
					<td><?php echo $apptdata_list->reg_date; ?></td>
					<td><?php echo ""; ?></td>
					
                    
						
					
				
				</tr>

				
			
				
			</tbody>
			
		</table>
	
	</div>
</div>
<div class="row">
<div class="card">
		<?php $atts = array('id' => 'submitPatientAppointment', 'class' => 'form-submitPatientAppointment', 'role' => 'form'); ?>
        <?php echo form_open('record/Dashboard/submitPatientAppointment', $atts); ?>
        <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-med"></i>
                        </div>
                            <select name="pf_no" class="form-control" data-validate="required" id="pf_no" onchange="return get_class_sections(this.value)">
                              <option value="">select a doctor</option>
                              <?php 
                                
                                foreach($apptdatadoc as $row):
                                    ?>
                                    <option value="<?php echo $row->pfNo; ?>" >
                                            <?php echo $row->fname . " " . $row->sname . " " . "{".$row->doctorspecialization ."}". " - " . $row->pfNo  ; ?>
                                    </option>
                                <?php
                                endforeach;
                              ?>
                             </select>
                        </div> 
                    </div>



                     <input type="hidden" name="doctor_name"  value="<?php echo $apptdata_list->patient_id; ?>" />
        			 <input type="hidden" name="patient_id"  value="<?php echo $apptdata_list->patient_id; ?>" />
        			 <input type="hidden" name="appt_date"  value="<?php echo date("Y-m-d H:i:s"); ?>" />
        			 <input type="hidden" name="patient_name"  value="<?php echo $apptdata_list->fname . " " .$apptdata_list->sname; ?>" />


        <div class="form-group">
              <button type="submit" class="btn btn-primary btn-block btn-login">
               <i class="fa fa-login"></i>
                submit Appointment 
               </button>
        </div>

</div>		
</div>

<?php endforeach; ?>

			<!-- Ignite Data Tables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#bookpat").dataTable();
                	});
                </script>

