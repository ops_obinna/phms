<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Update Patient Record</li>
                        </ol>
                    </div>
                    
                </div>


                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Registration form</h4>
                                <hr>


									<!-- FORM STARTS HERE-->




		<?php $atts = array('id' => 'updatepatient', 'class' => 'form-updatepatient', 'role' => 'form'); ?>
        <?php echo form_open('record/Dashboard/updatePatient/'.$item->patient_id, $atts); ?>

        <?php echo validation_errors('<p class="alert alert-danger">'); ?>
        <?php if($this->session->flashdata('success')) : ?>
        <?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
        <?php endif; ?>
        <?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
      
                
                <div class="form-group">

                    <?php $data = array(
                            'name'          => 'sname',
                            'id'            => 'sname',
                            'placeholder'   => 'Patient Surname',
                            'class'         => 'form-control',
                            'value'         =>  $item->sname

                        ) ;?>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>
                
                <div class="form-group">

                    <?php $data = array(
                            'name'          => 'fname',
                            'id'            => 'fname',
                            'placeholder'   => 'Patient First Name',
                            'class'         => 'form-control',
                            'value'         =>  $item->fname

                        ) ;?>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>

                <div class="form-group">

                    <?php $data = array(
                            'name'          => 'dob',
                            'id'            => 'dob',
                            'placeholder'   => 'Date of Birth',
                            'class'         => 'form-control',
                            'value'         =>  $item->dob

                        ) ;?>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>

                

                 <div class="form-group">
					 <div class="input-group">
					 	<div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
							<select name="gender" class="form-control" palceholder="Gender" data-validate="required" id="gender" onchange="return get_class_sections(this.value)">
                              <option value="<?php echo $item->gender; ?>"><?php echo $item->gender; ?></option>
                              <option value="Male">Male</option>

                              <option value="Female">Female</option>
                             </select>
						</div> 
					</div>


                    <div class="form-group">

                    <?php $data = array(
                            'name'          => 'phone',
                            'id'            => 'phone',
                            'placeholder'   => 'Mobile Phone Contact',
                            'class'         => 'form-control',
                            'value'         =>  $item->phone

                        ) ;?>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>

                <div class="form-group">

                    <?php $data = array(
                            'name'          => 'email',
                            'id'            => 'email',
                            'placeholder'   => 'Email Address',
                            'class'         => 'form-control',
                            'value'         =>  $item->email

                        ) ;?>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>

                	<div class="form-group">
					 <div class="input-group">
					 	<div class="input-group-addon">
                            <i class="fa fa-question-circle"></i>
                        </div>
							<select name="category" class="form-control" data-validate="required" id="category" onchange="return get_class_sections(this.value)">
                              <option value="<?php echo $item->category; ?>"><?php echo $item->category; ?></option>
                              <option value="Student">Student</option>
                              <option value="Staff">Staff</option>
                              <option value="Others">Others</option>
                             </select>
						</div> 
					</div>



                    <div class="form-group">

                    <?php $data = array(
                            'name'          => 'patient_id',
                            'id'            => 'patient_id',
                            'placeholder'   => 'Patient ID',
                            'class'         => 'form-control',
                            'value'         =>  $item->patient_id

                        ) ;?>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>

                	 <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-home"></i>
                        </div>
                            <select name="state" class="form-control" data-validate="required" id="state" onchange="return get_class_sections(this.value)">
                              <option value="<?php echo $item->state; ?>"><?php echo $item->statename; ?></option>
                              <?php 
                                
                                foreach($state as $row):
                                    ?>
                                    <option value="<?php echo $row['id']; ?>" >
                                            <?php echo $row['statename']; ?>
                                    </option>
                                <?php
                                endforeach;
                              ?>
                             </select>
                        </div> 
                    </div>


                    <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-home"></i>
                        </div>
                            <select name="city" class="form-control" data-validate="required" id="city" onchange="return get_class_sections(this.value)">
                              <option value="<?php echo $item->city; ?>"><?php echo $item->lganame; ?></option>
                              
                             </select>
                        </div> 
                    </div>

                    <div class="form-group">

                    <?php $data = array(
                            'name'          => 'nok_fullname',
                            'id'            => 'nok_fullname',
                            'placeholder'   => 'Next of Kin Fullname',
                            'class'         => 'form-control',
                            'value'         =>  $item->nok_fullname

                        ) ;?>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>

                <div class="form-group">

                    <?php $data = array(
                            'name'          => 'nok_phone',
                            'id'            => 'nok_phone',
                            'placeholder'   => 'Next of Kin Phone Contact',
                            'class'         => 'form-control',
                            'value'         =>  $item->nok_phone

                        ) ;?>
                      <div class="input-group">
                        
                        <div class="input-group-addon">
                            <i class="fa fa-user"></i>
                        </div>
                        
                        <?php echo form_input($data); ?>
                    </div>
                    
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login">
                        <i class="fa fa-login"></i>
                        Update Patient Record
                    </button>
                </div>
                    
            <?php echo form_close(); ?>
										
		<!--FORM ENDS HERE -->
        </div>
       </div>
      </div>
  </div>


     <!--this script inignites the datepicker-->
     <script type="text/javascript">
        $(function() {
        $("#dob").datepicker({ dateFormat: "yy-mm-dd" });
        });
    </script>

