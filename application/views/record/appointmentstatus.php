<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Appointment Status</li>
                        </ol>
                    </div>
                    
                </div>

                <br/>

		<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>

		<div class="card">
			
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="aptstat">
			<thead  class="active">
				<tr>
					
					
					<th>Doctor Name</th>
					<th>PF NO</th>
					<th>Patient Name</th>
					<th>Appointment Date and Time</th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_apt as $apt_list) : ?>
				<tr>
                    
					
					
					<td><?php echo $apt_list->sname . " " . $apt_list->fname; ?></td>
					<td><?php echo $apt_list->pf_no; ?></td>
					<td><?php echo $apt_list->patient_name; ?></td>
					<td><?php echo $apt_list->appt_date; ?></td>
					
				
					
				
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
	</div>


			<!-- Ignite DataTables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#aptstat").dataTable();
                	});
                </script>


