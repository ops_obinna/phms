<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Verify Patient</li>
                        </ol>
                    </div>
                    
                </div>


                 <br />

		<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>
		
		<div class="card">
			
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="verifypatient">
			<thead  class="active">
				<tr>
					
					
					<th>Names</th>
					<th>Age</th>
					<th>Category</th>
					<th>Identity</th>
					<th>Sex</th>
					<th>Phone No</th>
					<th>Next of Kin Contact</th>				
					<th>Date of Registration</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_patient as $patient_list) : ?>
				<tr>
                    
					
					
					<td><?php echo $patient_list->sname . " " . $patient_list->fname; ?></td>
					<td><?php echo $patient_list->age; ?></td>
					<td><?php echo $patient_list->category; ?></td>
					<td><?php echo $patient_list->patient_id; ?></td>
					<td><?php echo $patient_list->gender; ?></td>
					<td><?php echo $patient_list->phone; ?></td>
					<td><?php echo $patient_list->nok_phone; ?></td>
					<td><?php echo $patient_list->reg_date; ?></td>
					<td>
						<button style="width: 90%; text-align: center;background-color: white;cursor: pointer;display: block;">
						 <div class="btn-group">
                                                           
                                            <?php echo anchor('record/Dashboard/bookPatient/' .$patient_list->patient_id.'','     Book Patient', 'class="fa fa-edit"'); ?>
                                      
                                </div>
                          </button>
						</td>
                    
						
					
				
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
	</div>
	


			<!-- Ignite Data Tables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#verifypatient").dataTable();
                	});
                </script>

