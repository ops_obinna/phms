<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">List of Registered Patients</li>
                        </ol>
                    </div>
                    
                </div>

                <br />

		<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>

		<div class="card">
			
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="viewpatient">
			<thead  class="active">
				<tr>
					
					
					<th>Names</th>
					<th>Age</th>
					<th>Category</th>
					<th>Identity</th>
					<th>Sex</th>
					<th>Phone No</th>
					<th>Next of Kin Contact</th>				
					<th>Date of Registration</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($view_patient as $patient_list) : ?>
				<tr>
                    
					
					
					<td><?php echo $patient_list->sname . " " . $patient_list->fname; ?></td>
					<td><?php echo $patient_list->age; ?></td>
					<td><?php echo $patient_list->category; ?></td>
					<td><?php echo $patient_list->patient_id; ?></td>
					<td><?php echo $patient_list->gender; ?></td>
					<td><?php echo $patient_list->phone; ?></td>
					<td><?php echo $patient_list->nok_phone; ?></td>
					<td><?php echo $patient_list->reg_date; ?></td>
					<td>
						 <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- EDIT CLASS REPS LINKS  -->
                                        <button style="width: 70%; text-align: center;background-color: white;cursor: pointer;display: block;">
                                        <li>
                                            <?php echo anchor('record/Dashboard/editPatient/' .$patient_list->patient_id.'','     Edit', 'class="fa fa-edit"'); ?>
                                                                                                  
                                        </li>
                                    	</button>

                                         <li>

                                        
                                                                                                  
                                        </li>
                                        <!-- DELETE PROFILE LINK -->
                                        <button style="width: 70%; text-align: center;background-color: white;cursor: pointer;display: block;">
                                        <li>
                                            
                                            <?php echo anchor('record/Dashboard/deletePatient/' .$patient_list->patient_id.'','    Delete','class="fa fa-trash"'); ?> 
                                                
                                                                                              
                                        </li>
                                        </button>
                                        
                                    </ul>
                                </div>
						</td>
                    
						
					
				
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
	
	</div>


			<!-- Ignite Data Tables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#viewpatient").dataTable();
                	});
                </script>

