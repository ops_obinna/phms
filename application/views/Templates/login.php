<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title>FUT-MINNA| MEDICAL CENTER</title>
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png">
    <link rel="stylesheet" href="assets/js-neon/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/neon-forms.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skins/blue.css">


    <script src="<?php echo base_url(); ?>assets/js-neon/jquery-1.11.0.min.js"></script>
    

    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    

</head>
<body class="page-body page-fade login-page login-form-fall" data-url="http://neon.dev">
<!-- This is needed when you send requests via Ajax -->
<script type="text/javascript">
var baseurl = '<?php echo base_url();?>';
</script>
   
<div class="login-container">
    
    <div class="login-content">
            
            <a href="index.html" class="logo">
                <img src="<?php echo base_url(); ?>assets/images/logos.png" width="100%" alt="" />
            </a>            
    </div>
            
    <div class="login-form">
        <div class="login-content">
            
    <p><strong>Enter your Login details to Access the E-portal</strong> .</p>

   <!-- Load Main View-->
   


      <?php $this->load->view($main); ?>


   <!--END LOAD VIEW !-->
                
        </div>
    </div>
</div>


    <!-- Bottom scripts (common) -->
    <script src="<?php echo base_url(); ?>assets/js-neon/gsap/main-gsap.js"></script>
    <script src="<?php echo base_url(); ?>assets/js-neon/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js-neon/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/js-neon/joinable.js"></script>
    <script src="<?php echo base_url(); ?>assets/js-neon/resizeable.js"></script>
    <script src="<?php echo base_url(); ?>assets/js-neon/neon-api.js"></script>
    <script src="<?php echo base_url(); ?>assets/js-neon/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js-neon/neon-login.js"></script>


    <!-- JavaScripts initializations and stuff -->
    <script src="<?php echo base_url(); ?>assets/js-neon/neon-custom.js"></script>


    <!-- Demo Settings -->
    <script src="<?php echo base_url(); ?>assets/js-neon/neon-demo.js"></script>

</body>
</html>