<div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Manage Patient</li>
                        </ol>
                    </div>
                    
                </div>

                <div class="row">
                    <!-- Column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title"style="color:red;">NOTE</h4>
                                <div class="text-right">
                                    <h5 class="font-light m-b-0"><i class="fa fa-user-md" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted">Below are list of patients attended to so far: you can enter the patient_id in the search box area of the table for easy retrieval and then compare dates for most recent diagnosis</span></h5>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <br />

		<?php if($this->session->flashdata('success')) : ?>
		<?php echo  '<div class="alert alert-success">'.$this->session->flashdata('success').'</div>'; ?>
		<?php endif; ?>
		<?php if($this->session->flashdata('error')) : ?>
        <?php echo '<div class="alert alert-danger">' . $this->session->flashdata('error'). '</div>'; ?>
        <?php endif; ?>
		
		<div class="card">
			<?php if($viewpatient) : ?>
		<table class="table table-bordered datatable table-hover table-condensed table-striped" id="currentHistory" style="font-size:0.9em;">
			<thead  class="active">
				<tr>
					<th>ID</th>
					<th>Patient Name</th>
					<th>Patient ID</th>
					<th>Diagnosis</th>
					<th>Date</th>
					<th>Recomendation</th>
					<th>Doc Name</th>
					<th>Doc ID</th>
					<th>Action</th>
					<th>Complete</th>
					
				</tr>
			</thead>
			<tbody>
				<?php foreach ($viewpatient as $patient_list) : ?>
				<tr>
					<td><?php echo $patient_list->id; ?></td>
                	<td><?php echo $patient_list->patient_name; ?></td>
					<td><?php echo $patient_list->patient_id; ?></td>
					<td><?php echo $patient_list->diagnosis; ?></td>
					<td><?php echo $patient_list->prescription_date; ?></td>
					<td><?php echo $patient_list->nurse_prescription; ?></td>
					<td><?php echo $patient_list->doctor_name; ?></td>
					<td><?php echo $patient_list->doctor_id; ?></td>
					
				<td>
					<div class="btn-group">
                                                  
                        <button>
                        <a href="administerPrescription/<?php echo $patient_list->patient_id; ?>/<?php echo $patient_list->prescription_date; ?>" title="News title">Treat</a>
                        </button>
                                  
                        </div>
				</td>
				<td>
					<div class="btn-group">
                                                  
                        <button>
                        <a href="nurseAdministerComplete/<?php echo $patient_list->patient_id; ?>/<?php echo $patient_list->prescription_date; ?>" title="News title">Done</a>
                        </button>
                                  
                        </div>
				</td>
				</tr>

				
			<?php endforeach; ?>
				
			</tbody>
			
		</table>
		<?php else : ?>
	<div class="row">
                    <!-- Column -->
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-block">
                                <div class="text-right">
                                    <h2 class="font-light m-b-0"><i class="fa fa-users" aria-hidden="true"></i>
                                        <?php echo "|"; ?><span class="text-muted">No patient waiting To be Treated</span></h2>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
	<?php endif; ?>
	
	</div>
	


			<!-- Ignite Data Tables-->
	 		<script type="text/javascript">
                	$(function(){

                		$("#currentHistory").dataTable();
                	});
                </script>




                