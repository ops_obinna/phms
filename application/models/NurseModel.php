<?php

class NurseModel extends CI_Model{

 //model to add log 
  public function addLog($data){
    $this->table = 'system_log';
    $this->db->insert($this->table, $data);

  }

   //This function selects patients that have been diagnosed for the nurse to view 
    public function viewPatient(){
        
        $this->db->select('*');
        $this->db->from('patient_health_record');
        $this->db->where('nurse_prescription is NOT NULL', NULL, FALSE);
        $this->db->where('nurse_done ', 0 );
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result();       

    }
  
  //this function will select patient that will receive treatment from nurse
    public function nurseViewForTreatment($patient_id, $prescription_date){

        $this->db->select('*');
        $this->db->from('patient_health_record');
        $this->db->where('patient_id', $patient_id);
        $this->db->where('prescription_date', $prescription_date);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->result(); 

    }

    //this function updates the patient_health_record based on nurse treation i.e injection update
    public function NurseInjectionUpdate($data, $patient_id, $prescription_date){
        $this->table = 'patient_health_record'; //defining the table name
        $this->db->where('patient_id', $patient_id);
        $this->db->where('prescription_date', $prescription_date);
        $this->db->update($this->table, $data); //performing insert operation 

    }

     //this function updates the patient_health_history based on nurse treation i.e injection update
    public function HistoryInjectionUpdate($history, $patient_id, $prescription_date){
        $this->table = 'patient_health_history'; //defining the table name
        $this->db->where('patient_id', $patient_id);
        $this->db->where('diagnosis_date', $prescription_date);
        $this->db->update($this->table, $history); //performing insert operation 

    }
    //this function helps the nurse not see the patient whose treatment have been through with
    public function nurseAdministerComplete($data, $patient_id, $prescription_date){
        $this->table = 'patient_health_record'; //defining the table name
        $this->db->where('patient_id', $patient_id);
        $this->db->where('prescription_date', $prescription_date);
        $this->db->update($this->table, $data); //performing insert operation 
    }



  }
