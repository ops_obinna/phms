<?php
//** MODEL CLASS FOR LOGIN**//
class LabModel extends CI_Model{

    //model to add log 
    public function addLog($data) {
    $this->table = 'system_log';
    $this->db->insert($this->table, $data);

    }

    //this function loads model for general lab test
    public function viewGeneralLabPatients(){
        $this->db->select('*');
        $this->db->from('general_lab_request_form');
        $this->db->where('investigated', 0);
        $query = $this->db->get();
        return $query->result();
    }


    //this model loads patiens for microbiologist test 
    public function viewMicrobiologistPatient(){
        $this->db->select('*');
        $this->db->from('microbiology');
        $this->db->where('investigated', 0);
        $query = $this->db->get();
        return $query->result();
    }
  
  //this function loads data for the microbiologist form
    public function loadMicrobiologistForm($patient_id){
        $this->db->select('*');
        $this->db->from('microbiology');
        $this->db->where('investigated', 0);
        $this->db->where('patient_id', $patient_id);
        $query = $this->db->get();
        return $query->result();
    }

    //THIS FUNCTION UPDATE THE MICROBIOLOGIST 
    public function submitMicroBiologyUpdate($data, $patient_id){
        $this->table = 'microbiology';
        $this->db->where('patient_id', $patient_id);
        $this->db->update($this->table, $data); 

    }

    //This model loads the General Request form for a patient
    public function loadPatientGenLabForm($patient_id){
        $this->db->select('*');
        $this->db->from('general_lab_request_form');
        $this->db->where('patient_id', $patient_id);
        $this->db->where('investigated', 0);
        $query = $this->db->get();
        return $query->result();
    }

    //This function selects the patient whose id is listed 
    //and forward it to the Report form for the lab scientis to continue
    public function loadPatientReportForm($patient_id){
        $this->db->select('*');
        $this->db->from('patients_table');
        $this->db->where('patient_id', $patient_id);
        $query = $this->db->get();
        return $query->result();
    }


    public function fetcDocData($doc_id){
        $this->db->select('*');
        $this->db->from('staff');
        $this->db->where('pfNo', $doc_id);
        $query = $this->db->get();
        return $query->result();

    }

    public function submitGenLabRequestInsert($data){
        $this->table = 'lab_report_form';
        $this->db->insert($this->table, $data);
    }

    public function submitGenLabRequestUpdate($data, $patient_id){
        $this->table = 'lab_report_form';
        $this->db->where('patient_id', $patient_id);
        $this->db->update($this->table, $data); 
        
    }

    public function patientTested($done, $patient_id){
        $this->table = 'general_lab_request_form';
        $this->db->where('patient_id', $patient_id);
        $this->db->update($this->table, $done);
    }


  }
