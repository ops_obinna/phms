<?php
//** MODEL CLASS FOR LOGIN**//
class AdminModel extends CI_Model{


  //model to add log 
  public function addLog($data){
    $this->table = 'system_log';
    $this->db->insert($this->table, $data);

  }

  //this function select all log from the system log table
  public function getUserLog(){
  $this->db->select()->from('system_log');
  $query = $this->db->get();
  return $query->result();
  }

//this function selects and return counts of staff in the DB to the calling controller
  public function staffCount()
  {
    $query = $this->db->get('staff');
    return $query->num_rows();
  }

  public function registerStaff($data){
  	$this->table = 'staff'; //defining the table name
	$this->db->insert($this->table, $data); //performing insert operation 

  }
  //this function select all staff records
  public function getStaff(){

        $this->db->select('*');
        $this->db->from('staff');
        $this->db->join('state', 'state.id = staff.state');
        $this->db->join('lga', 'lga.id = staff.city');
        $query = $this->db->get();
        return $query->result();

  }

  //this function delete staff
  public function deleteStaff($pfNo){
 		$this->table = 'staff';
 		$this->db->where('pfNo', $pfNo);
 		$this->db->delete($this->table);

 	}
 	//this function update staff records
 	public function updateStaff($pfNo, $data){
 		$this->table = 'staff';
 		$this->db->where('pfNo', $pfNo);
 		$this->db->update($this->table, $data);
 	}

 	//this function gets the record for the staff which it id is send 
 	public function getStaffByID($pfNo){
 		$this->db->select('*');
    $this->db->from('staff');
    $this->db->where('staff.pfNo', $pfNo);
    $this->db->join('state', 'state.id = staff.state');
    $this->db->join('lga', 'lga.id = staff.city');
	  $query = $this->db->get();
	  return $query->row();


 	}

 	//this function update staff record  which its ID is sent
 	public function updateStaffByID($pfNo, $data){
 		$this->table = 'staff';
 		$this->db->where('pfNo', $pfNo);
 		$this->db->update($this->table, $data);
 	}



  //get the state 
  public function getState(){
    $this->db->select('id,statename');  
    $this->db->from('state');  
    $query = $this->db->get();
    return $query->result_array();
  }

  //get city by ID
   //fill your city dropdown depending on the selected state  
   public function getCityByState($stateid=string)  
   {  
      $this->db->select('id,lganame');  
      $this->db->from('lga');  
      $this->db->where('stateid',$stateid); 
      $this->db->order_by('lganame', 'ASC');
      $query = $this->db->get();  
      return $query->result();  
   }  
    
    
}//END OF CLASSS