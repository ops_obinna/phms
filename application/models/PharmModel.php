<?php

class PharmModel extends CI_Model{

 //model to add log 
  public function addLog($data){
    $this->table = 'system_log';
    $this->db->insert($this->table, $data);

  }

   //This function selects patients that have been diagnosed for the nurse to view 
    public function viewPatient(){
        
        $this->db->select('*');
        $this->db->from('patient_health_record');
        $this->db->where('pharmacy_prescription is NOT NULL', NULL, FALSE);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        return $query->result();       

    }
  
  //this function will select patient that will receive treatment from pharmacist
    public function pharmViewForTreatment($patient_id, $prescription_date){

        $this->db->select('*');
        $this->db->from('patient_health_record');
        $this->db->where('patient_id', $patient_id);
        $this->db->where('prescription_date', $prescription_date);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->result(); 

    }


   

    //this function updates the patient_health_record based on pharmacist treation i.e drugs update
    public function PharmDrugUpdate($data, $patient_id, $prescription_date){
        $this->table = 'patient_health_record'; //defining the table name
        $this->db->where('patient_id', $patient_id);
        $this->db->where('prescription_date', $prescription_date);
        $this->db->update($this->table, $data); //performing insert operation 

    }

     //this function updates the patient_health_history based on pharmacist treation i.e drugs update
    public function HistoryDrugUpdate($history, $patient_id, $prescription_date){
        $this->table = 'patient_health_history'; //defining the table name
        $this->db->where('patient_id', $patient_id);
        $this->db->where('diagnosis_date', $prescription_date);
        $this->db->update($this->table, $history); //performing insert operation 

    }



  }
