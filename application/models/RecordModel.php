<?php
//** MODEL CLASS FOR LOGIN**//
class RecordModel extends CI_Model{

  //model to add log 
  public function addLog($data){
    $this->table = 'system_log';
    $this->db->insert($this->table, $data);

  }

  
  //get the state 
  public function getState(){
    $this->db->select('id,statename');  
    $this->db->from('state');  
    $query = $this->db->get();
    return $query->result_array();
  }

  //get city by ID
   //fill your city dropdown depending on the selected state  
   public function getCityByState($stateid=string)  
   {  
      $this->db->select('id,lganame');  
      $this->db->from('lga');  
      $this->db->where('stateid',$stateid); 
      $this->db->order_by('lganame', 'ASC');
      $query = $this->db->get();  
      return $query->result();  
   }  

   //This funtion register a patient
   public function registerPatient($data){
    $this->table = 'patients_table'; //defining the table name
    $this->db->insert($this->table, $data); //performing insert operation 

  }


  //this function select all patient records
  public function getPatient(){

        $this->db->select('*');
        $this->db->from('patients_table');
        $this->db->join('state', 'state.id = patients_table.state');
        $this->db->join('lga', 'lga.id = patients_table.city');
        $query = $this->db->get();
        return $query->result();

  }

//this function delete patient
  public function deletePatient($patient_id){
    $this->table = 'patients_table';
    $this->db->where('patient_id', $patient_id  );
    $this->db->delete($this->table);

  }

  //this function gets the record for the  which it id is send 
  public function getPatientByID($patient_id){
    $this->db->select('*');
    $this->db->from('patients_table');
    $this->db->where('patients_table.patient_id', $patient_id);
    $this->db->join('state', 'state.id = patients_table.state');
    $this->db->join('lga', 'lga.id = patients_table.city');
    $query = $this->db->get();  
    return $query->row();
  }

  //this function update patient record  which its ID is sent
  public function updatePatientByID($patient_id, $data){
    $this->table = 'patients_table';
    $this->db->where('patient_id', $patient_id);
    $this->db->update($this->table, $data);
  }

    //this function update patient records
  public function updatePatient($patient_id, $data){
    $this->table = 'patients_table';
    $this->db->where('patient_id', $patient_id);
    $this->db->update($this->table, $data);
  }

  //This function is used to get patient data and doctor data to be submitted to the database
  public  function getApptData($patient_id){
    $this->db->select('*');
    $this->db->from('patients_table');
    $this->db->where('patient_id', $patient_id);
    $query = $this->db->get();  
    return $query->result();
  }

  //this function selects doctor to be booked and submitted to the appointment table 
  public function getApptDataDoc(){
    $this->db->select('*');
    $this->db->from('staff');
    $this->db->where('specialization', 'Doctor');
    $query = $this->db->get();  
    return $query->result();
  }
  
  public function bookPatient($data){
    $this->table = 'appointment'; 
    $this->db->insert($this->table, $data);
  }

  public function getAppointment(){
        $this->db->select('*');
        $this->db->from('appointment');
        $this->db->join('patients_table', 'patients_table.patient_id = appointment.patient_id');
        $this->db->join('staff', 'staff.pfNo = appointment.pf_no');
        $query = $this->db->get();
        return $query->result();


  }

}//END CLASSS RecordModel