<?php
//** MODEL CLASS FOR LOGIN**//
class DoctorModel extends CI_Model{

 //model to add log 
  public function addLog($data){
    $this->table = 'system_log';
    $this->db->insert($this->table, $data);

  }
  //this function only inserts patient_id into the patient_record table
  public function preInsertRecord($data){
    $this->table = 'patient_health_record'; //defining the table name
    $this->db->insert($this->table, $data); //performing insert operation 
  }


	public function viewHistory(){
		$this->db->select('*');
        $this->db->from('appointment');
        $this->db->join('staff', 'staff.pfNo = appointment.pf_no');
        $this->db->order_by('appt_id', 'DESC');
        $query = $this->db->get();
        return $query->result();

	}


	public function currentAppt($pfNo){
        $diagnosed=0;
		$this->db->select('*');
        $this->db->from('appointment');
        $this->db->where('pf_no', $pfNo);
        $this->db->where('diagnosed', $diagnosed);
        $this->db->join('staff', 'staff.pfNo = appointment.pf_no');
        $this->db->order_by('appt_id', 'DESC');
        $query = $this->db->get();
        return $query->result();

	}

    //this function selects patient for diagnosis by Doctor
    public function selectPatientById($patientId){
        $this->db->select('*');
        $this->db->from('patients_table');
        $this->db->where('patients_table.patient_id', $patientId);
        $this->db->join('patient_health_record', 'patient_health_record.patient_id = patients_table.patient_id');
        $query = $this->db->get();
        return $query->result();
    }

    //submit the diagnosis by the doctor
    public function submitDiagnosis($data, $patient_id){
    $this->table = 'patient_health_record'; //defining the table name
    $this->db->where('patient_id', $patient_id);
    $this->db->update($this->table, $data); //performing insert operation 
    }
 
 
 //this function selects the patient record who have been diagnosed by the doctor
    public function selectPatientRecord($patient_id){
        $this->db->select('*');
        $this->db->from('patient_health_record');
        $this->db->where('patient_id', $patient_id);
        $query = $this->db->get();
        return $query->result();
    }

    //this function update a particular column where the id is supplied
    public function patientSeen($diagnosed, $patient_id){
    //$diagnosed = 0;    
    $this->table = 'appointment';
    $this->db->where('patient_id', $patient_id);
    $this->db->where('diagnosed', 0);
    $this->db->update($this->table, $diagnosed);
    }


    //this function selects patient diagnosed by doctor by ID
    public function selectPatientDiagnosisById($patient_id){
        $this->db->select('*');
        $this->db->from('patient_health_record');
        $this->db->where('patient_health_record.patient_id', $patient_id);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->result();
    }

    //this function submits the doctor prescription for the nurse
    public function submitNursePrescription($data, $patient_id){
        $this->table = 'patient_health_record';
        $this->db->where('patient_id', $patient_id);
        $this->db->update($this->table, $data);
    }

    //This function submits the doctor prescription for the Pharmacist
    public function submitPharmPrescription($data, $patient_id){
        $this->table = 'patient_health_record';
        $this->db->where('patient_id', $patient_id);
        $this->db->update($this->table, $data);
    }

    //This function find if a patient exist in the Lag General Request Database 
    public function findPatientinLabRecord($id){
        $this->db->select('*');
        $this->db->from('general_lab_request_form');
        $this->db->where('general_lab_request_form.patient_id', $id);
        $query = $this->db->get();
        return $query->result();
    }

    //this function inserts to the General Lab form if no duplicate patient Id
    public function submitGenLabRequestInsert($data){
        $this->table = 'general_lab_request_form';
        $this->db->insert($this->table, $data);

    }

    //this Function update the General Lab Form is a duplicate Patient ID exist
    public function submitGenLabRequestUpdate($data, $patient_id){
        $this->table = 'general_lab_request_form';
        $this->db->where('patient_id', $patient_id);
        $this->db->update($this->table, $data);        

    }

    //this function inserts into the Microbiology table
    public function submitMicroBiologyInsert($data){
        $this->table = 'microbiology';
        $this->db->insert($this->table, $data);

    }

    //this function inserts into the Microbiology table
    public function submitMicroBiologyUpdate($data, $patient_id){
        $this->table = 'microbiology';
        $this->db->where('patient_id', $patient_id);
        $this->db->update($this->table, $data);        

    }

    //selects patient by appointment ID
    public function selectSinglePatient($appt_id){
        $this->db->select('*');
        $this->db->from('appointment');
        $this->db->where('appt_id', $appt_id);
        $this->db->join('staff', 'staff.pfNo = appointment.pf_no');
        $query = $this->db->get();
        return $query->result();
    }

    //this model ssubmites data into the Patient_health_history Table
    public function submitHealthHistory($history){
        $this->table = 'patient_health_history';
        $this->db->insert($this->table, $history);
    }

    //this function submits nurse prescription to the patient health history table
    public function submitNursePrescriptionHistory($history, $prescription_date){
        $this->table = 'patient_health_history';
        $this->db->where('diagnosis_date', $prescription_date);
        $this->db->update($this->table, $history);
    }

    //
    public function submitPharmPrescriptionHistory($history, $prescription_date){
        $this->table = 'patient_health_history';
        $this->db->where('diagnosis_date', $prescription_date);
        $this->db->update($this->table, $history);   
    }

    //this function select patients with History in the Medical Center
    public function patientsWithHistory(){
        $this->db->select('*');
        $this->db->from('patient_health_record');
        $query = $this->db->get();
        return $query->result();
    }

    //this model select all history for a patient
    public function viewHistorySinglePatient($patient_id){
        $this->db->select('*');
        $this->db->from('patient_health_history');
        $this->db->where('patient_id', $patient_id);
        $query = $this->db->get();
        return $query->result();
    }

    //this model function fetches patients sent to microbiologist
    public function viewMicroBiologist(){
        $this->db->select('*');
        $this->db->from('microbiology');
        $this->db->where('investigated', 1);
        $query = $this->db->get();
        return $query->result();

    }

    //this model function feteches patients sent to general lab test
    public function viewGenLabReport($doc_id){
        $this->db->select('*');
        $this->db->from('general_lab_request_form');
        $this->db->where('investigated', 1);
        $this->db->where('doctor_id', $doc_id);
        $query = $this->db->get();
        return $query->result();
    }

    //this funcion will load microbiologist report form of the patient choosen by the doctor
    public function viewMicrobiologistReport($patient_id, $date_investigated){
        $this->db->select('*');
        $this->db->from('microbiology');
        $this->db->where('patient_id', $patient_id);
        $this->db->where('date_investigated', $date_investigated);
        $query = $this->db->get();
        return $query->result();
    }

    //this function will load the general lab form initially submitted by doctor
    public function viewGenLabReportDoctor($patient_id, $submitted_date){
        $this->db->select('*');
        $this->db->from('general_lab_request_form');
        $this->db->where('patient_id', $patient_id);
        $this->db->where('submitted_date', $submitted_date);
        $query = $this->db->get();
        return $query->result();
    }

    //this model will load the Lab report form for a particular patient
    public function viewPatientLabReport($patient_id){
        $this->db->select('*');
        $this->db->from('lab_report_form');
        $this->db->where('patient_id', $patient_id);
        $query = $this->db->get();
        return $query->result();
    }

    //this function selects patients from the Health Records 
    public function viewPatientsFromHealthRecords($doc_id){
        $this->db->select('*');
        $this->db->from('patient_health_record');
        $this->db->where('doctor_id', $doc_id);
        $query = $this->db->get();
        return $query->result();
    }
    //This function add 0 to the nurse_done field of the patient_health_record table
    public function nurse_done($nursedone, $patient_id){
        $this->table = 'patient_health_record';
        $this->db->where('patient_id', $patient_id);
        $this->db->update($this->table, $nursedone);
    }

    //this function loads patients whose diagnosis failed 
    public function failedDiagnosis($pfNo){
        $this->db->select('*');
        $this->db->from('appointment');
        $this->db->where('diagnosed', 0);
        $this->db->where('pf_no', $pfNo);
        $query = $this->db->get();
        return $query->result();
    }

  }
