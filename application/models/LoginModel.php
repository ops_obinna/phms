<?php
//** MODEL CLASS FOR LOGIN**//
class LoginModel extends CI_Model{

    //admin login model function
    public function loginAdmin($email) {
           $this->db->select('*');
           $this->db->from('admin');
           $this->db->where('email', $email);
           $this->db->limit(1);
           $query = $this->db->get();
         
            if($query->num_rows() == 1){
                  return $query->result();
                  
            }else{
              return false;
                
           }
      }


      //doctor login model function 
      public function loginDoc($email) {
           $this->db->select('*');
           $this->db->from('staff');
           $this->db->where('email', $email);
           $this->db->where('specialization', 'Doctor');
           $this->db->limit(1);
           $query = $this->db->get();
         
            if($query->num_rows() == 1){
                  return $query->result();
                  
            }else{
              return false;
                
           }
      }

      //nurse login model function
       public function loginNurse($email) {
           $this->db->select('*');
           $this->db->from('staff');
           $this->db->where('email', $email);
           $this->db->where('specialization', 'Nurse');
           $this->db->limit(1);
           $query = $this->db->get();
         
            if($query->num_rows() == 1){
                  return $query->result();
                  
            }else{
              return false;
                
           }
      }

      //record officer logi model function
      public function loginRecords($email) {
           $this->db->select('*');
           $this->db->from('staff');
           $this->db->where('email', $email);
           $this->db->where('specialization', 'Record');
           $this->db->limit(1);
           $query = $this->db->get();
         
            if($query->num_rows() == 1){
                  return $query->result();
                  
            }else{
              return false;
                
           }
      }

      //lab scientist login model function
      public function loginLab($email) {
           $this->db->select('*');
           $this->db->from('staff');
           $this->db->where('email', $email);
           $this->db->where('specialization', 'Laboratory Scientist');
           $this->db->limit(1);
           $query = $this->db->get();
         
            if($query->num_rows() == 1){
                  return $query->result();
                  
            }else{
              return false;
                
           }
      }

      //pharmacist login model function
      public function loginPharm($email) {
           $this->db->select('*');
           $this->db->from('staff');
           $this->db->where('email', $email);
           $this->db->where('specialization', 'Pharmacist');
           $this->db->limit(1);
           $query = $this->db->get();
         
            if($query->num_rows() == 1){
                  return $query->result();
                  
            }else{
              return false;
                
           }
      }
    
}//END OF CLASSS