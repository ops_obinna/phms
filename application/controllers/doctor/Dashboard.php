<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	//This function load the view for the welcome page i.e the first page when you enter the URL
	public function index()
	{
				
		$this->template->load('doctor', 'default','homeview');
	}

//this function load all the patient that have been booked for appointment so far
	public function appointmentHistory()
	{

		$data['view_patient'] = $this->DoctorModel->viewHistory();
		$this->template->load('doctor', 'default','appointmenthistory', $data);
	}

//This function load the patients assigned to the particular doctor
	public function currentAppointment()
	{
		$pfNo = $this->session->userdata('pfNo');
		$data['view_patient'] = $this->DoctorModel->currentAppt($pfNo);
		$this->template->load('doctor', 'default','currentappointment', $data);
	}
	public function labResult()
	{
		$this->template->load('doctor', 'default','labresult');
	}
	//This function displays only patient with Medical History in the Medical Center
	public function medicalHistory()
	{
		$data['view_patient'] = $this->DoctorModel->patientsWithHistory();
		$this->template->load('doctor', 'default','medicalhistory', $data);
	}
	public function referPatient()
	{
		$doc_id = $this->session->userdata('pfNo');
		$data['view_patient'] = $this->DoctorModel->viewPatientsFromHealthRecords($doc_id);
		$this->template->load('doctor', 'default','referpatient', $data);
	}
	 

	 //this function loads the form for diagnosis by the doctor and populate the form
	public function diagnosePatient($patientId){
				
		//Checks if  a patient Exist in The Database Table patient_health_record if not it creates
		$this->db->select('*');
        $this->db->from('patient_health_record');
        $this->db->where('patient_id', $patientId);
        $query = $this->db->get();
        $exist = $query->num_rows();
        if(!$exist){
        	//this inserts a patient ID into the  patient_record table to enable 
			//subsequent update to the table afte doctor diagnosis
        	$patient_id['patient_id']	= $patientId;
			$this->DoctorModel->preInsertRecord($patient_id);

        }

		//This function select and displays the record of the patient whose ID is passed 
		$data['patientdata'] = $this->DoctorModel->selectPatientById($patientId);
		$this->template->load('doctor', 'default','diagnosepatient', $data);
	}

	

	//This function Submits the doctor diagnosis for the patient
	public function diagnosePatientDoc()
	{
		//setting validation rules
		$this->form_validation->set_rules('patient_name', 'patient_name','trim|required');
		$this->form_validation->set_rules('patient_id', 'patient_id', 'trim|required');
		$this->form_validation->set_rules('diagnosis', 'diagnosis', 'trim|required');
		
		$id=$this->input->post('patient_id');
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->diagnosePatient($id);

		  }else{

             
		//else is validation is true then create post array
            $patient_name         		= $this->input->post('patient_name');
            $patient_id          		= $this->input->post('patient_id');
            $diagnosis          		= $this->input->post('diagnosis');
                

            $data['patient_name'] 				= $patient_name;
			$data['patient_id']					= $patient_id;
			$data['diagnosis']					= $diagnosis;
			$data['doctor_id']					= $this->session->userdata('pfNo');
			$data['doctor_name']				= $this->session->userdata('fname') . " " . $this->session->userdata('sname');
			$data['	prescription_date']			= date("Y-m-d-H:i:s");
 
		$this->DoctorModel->submitDiagnosis($data, $patient_id);

		$diagnosed['diagnosed'] = "1";
		$this->DoctorModel->patientSeen($diagnosed, $patient_id);


		//collecting data array for patient_health_history table
		$history['doc_name']				= $this->session->userdata('fname') . " " . $this->session->userdata('sname');
		$history['doc_id']					= $this->session->userdata('pfNo');
		$history['patient_name'] 			= $patient_name;
		$history['patient_id'] 				= $patient_id;
		$history['patient_id'] 				= $patient_id;
		$history['diagnosis']				= $diagnosis;
		$history['diagnosis_date']			= date("Y-m-d-H:i:s");

		$this->DoctorModel->submitHealthHistory($history);

			//Activities log  array
    	$data = array(
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Diagnosed a patient with patent ID ' ." " . $patient_id
         ); 
    	//insert Activities
    	$this->DoctorModel->addLog($data);

    	

		$this->session->set_flashdata('success', 'Patient Diagnosed successfully');

		//redirect to the doctor to the forwardPatient Function for onward processing
		redirect(base_url() . "doctor/Dashboard/forwardPatient/" . $patient_id, 'refresh');
		//redirect('doctor/Dashboard/forwardPatient/'. $patient_id, 'refresh');

		}	

	}

	//this function select the record of the patient whide id is paased and use it 
	public function forwardPatient(){
		$patient_id = $this->uri->segment(4); 
		$data['forwarddiagnosis'] = $this->DoctorModel->selectPatientRecord($patient_id);
		$this->template->load('doctor', 'default','forwardpatient', $data);
	}

	//this function adds 1 to the appointment table to confirm that doctor have seen the patient
	public function patientSeen($appt_id){
		//$data['diagnosed'] = "1";
		//$this->DoctorModel->patientSeen($data, $appt_id);
		$data['view_patient'] = $this->DoctorModel->selectSinglePatient($appt_id);
		$this->template->load('doctor', 'default','diagnosesingle', $data);
	}



	//this form load the prevous health record of the patient 
	public function previousRecord($patient_id){
		$data['patientdata'] = $this->DoctorModel->selectPatientRecord($patient_id);
		$this->template->load('doctor', 'default','previousrecord', $data);
	}

	//This function will load the nurse form 
	public function nurseForm($patient_id){
		$data['patientdata'] = $this->DoctorModel->selectPatientDiagnosisById($patient_id);
		$this->template->load('doctor', 'default','nurseform', $data);

	}

	//this function loads the form for Pharmacist prescription
	public function pharmForm($patient_id){
		$data['patientdata'] = $this->DoctorModel->selectPatientDiagnosisById($patient_id);
		$this->template->load('doctor', 'default','pharmform', $data);
	}

	//This function loads the form for the General Laboratory Form 
	public function labForm($patient_id){
		$data['patientdata'] = $this->DoctorModel->selectPatientById($patient_id);
		$this->template->load('doctor', 'default','labform',$data);	
	}

	// the function loads the form for Laboratory Text Result 
	public function labTestResultForm()	{
		$this->template->load('doctor', 'default','labtestresultform');	

	}

	//this form load the Microbiologist form
	public function microForm($patient_id){
		$data['patientdata'] = $this->DoctorModel->selectPatientById($patient_id);
		$this->template->load('doctor', 'default','microform', $data);	

	}

	//This function submits the doctor instruction for the nurse 
	public function nurseFunction(){
		//setting validation rules
		$this->form_validation->set_rules('patient_id', 'patient_id','trim|required');
		$this->form_validation->set_rules('diagnosis', 'diagnosis', 'trim|required');
		$this->form_validation->set_rules('prescription_date', 'prescription_date', 'trim|required');
		$this->form_validation->set_rules('nurse_prescription', 'Prescription for Nurses', 'trim|required');
		$id=$this->input->post('patient_id');
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->nurseform($id);

		  }else{

             
			//else is validation is true then create post array
            $nurse_prescription         = $this->input->post('nurse_prescription');
            $patient_id          		= $this->input->post('patient_id');
            $prescription_date			= $this->input->post('prescription_date');  

            $data['nurse_prescription']  = $nurse_prescription;
			$this->DoctorModel->submitNursePrescription($data, $patient_id);

			//History table input
			$history['nurse_prescription']  = $nurse_prescription;
			$this->DoctorModel->submitNursePrescriptionHistory($history, $prescription_date);

			//This function add 0 to the nurse_done field of the patient_health_record table
			$nursedone['nurse_done'] = 0;
			$this->DoctorModel->nurse_done($nursedone, $patient_id);


		//Activities log  array
    	$data = array(
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Sent this patient with ID ' ." " . $patient_id . " ".'To the Nurse'
         ); 
    	//insert Activities
    	$this->DoctorModel->addLog($data);

    	

		$this->session->set_flashdata('success', 'Forwarded to Nurse successfully');

		//redirect to the doctor to the forwardPatient Function for onward processing
		redirect(base_url() . "doctor/Dashboard/forwardPatient/" . $patient_id, 'refresh');
		//redirect('doctor/Dashboard/forwardPatient/'. $patient_id, 'refresh');

		}	


	}

	//this function will submit the doctor instruction for the Phamacist
	public function pharmFunction(){

		//setting validation rules
		$this->form_validation->set_rules('patient_id', 'patient_id','trim|required');
		$this->form_validation->set_rules('diagnosis', 'diagnosis', 'trim|required');
		$this->form_validation->set_rules('prescription_date', 'prescription_date', 'trim|required');
		$this->form_validation->set_rules('pharmacy_prescription', 'Prescription for Pharmacist', 'trim|required');
		$id=$this->input->post('patient_id');
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->pharmform($id);

		  }else{

             

			//else is validation is true then create post array
            $pharmacy_prescription          = $this->input->post('pharmacy_prescription');
            $patient_id          			= $this->input->post('patient_id');
            $prescription_date				= $this->input->post('prescription_date');
            $data['pharmacy_prescription']  = $pharmacy_prescription;
			$this->DoctorModel->submitPharmPrescription($data, $patient_id);

			//this table add pharmacy prescription to the patient health history table
			//History table input
			$history['pharmacy_prescription']  = $pharmacy_prescription;
			$this->DoctorModel->submitPharmPrescriptionHistory($history, $prescription_date);

			//Activities log  array
    	$data = array(
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Sent this patient with ID ' ." " . $patient_id . " ".'To the Pharmacist'
         ); 
    	//insert Activities
    	$this->DoctorModel->addLog($data);

    	

		$this->session->set_flashdata('success', 'Forwarded to Pharmacist successfully');

		//redirect to the doctor to the forwardPatient Function for onward processing
		redirect(base_url() . "doctor/Dashboard/forwardPatient/" . $patient_id, 'refresh');
		//redirect('doctor/Dashboard/forwardPatient/'. $patient_id, 'refresh');

		}	


	}

	//This function submit the general Lab Request to the Lab scientist
	public function genLabRequest(){

		//setting validation rules
		$this->form_validation->set_rules('patient_id', 'patient_id','trim|required');
		$this->form_validation->set_rules('patient_sname', 'Patient Surname', 'trim|required');
		$this->form_validation->set_rules('patient_fname', 'Patient Othernames', 'trim|required');
		$this->form_validation->set_rules('patient_gender', 'Gender', 'trim|required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
		$this->form_validation->set_rules('specimen_type', 'Type of Specimen', 'trim|required');
		$this->form_validation->set_rules('prognosis', 'Prognosis', 'trim|required');
		$this->form_validation->set_rules('required_invest', 'Required Investigation', 'trim|required');
		$this->form_validation->set_rules('summary', 'Summary', 'trim|required');
		
		$id=$this->input->post('patient_id');

		
		//thsi query checks if thse patient is in the DB
		//if not it creates, it there is it updaes
		$this->db->select('*');
        $this->db->from('general_lab_request_form');
        $this->db->where('general_lab_request_form.patient_id', $id);
        $query = $this->db->get();
        $exist = $query->num_rows();

		
		//form validation to check form inputs
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->labForm($id);

		  }elseif(!$exist) { //if not exist is creates

		  	
            $patient_id          	= $this->input->post('patient_id');
            $doctor_id          	= $this->session->userdata('pfNo');
            $patient_sname          = $this->input->post('patient_sname');
            $patient_fname          = $this->input->post('patient_fname');
            $patient_gender         = $this->input->post('patient_gender');
            $dob          			= $this->input->post('dob');
            $specimen_type          = $this->input->post('specimen_type');
            $prognosis              = $this->input->post('prognosis');
            $required_invest        = $this->input->post('required_invest');
            $submitted_date         = date("Y-m-d-H:i:s");
            $summary                = $this->input->post('summary');
		  	  
		  	  //data array
			$data['patient_id']  		= $patient_id;
			$data['doctor_id']  		= $doctor_id;
			$data['surname']  			= $patient_sname;
			$data['other_names']  		= $patient_fname;
			$data['gender']  			= $patient_gender;
			$data['dob']  				= $dob;
			$data['specimen_type']  	= $specimen_type;
			$data['prognosis']  		= $prognosis;
			$data['required_invest']  	= $required_invest;
			$data['submitted_date']  	= $submitted_date;
			$data['summary']  			= $summary;
			$data['investigated']  		= 0;



			$this->DoctorModel->submitGenLabRequestInsert($data);

					//Activities log  array
		    	$data = array(
		        'pfNo'            =>  $this->session->userdata('pfNo'),
		        'email'           =>  $this->session->userdata('email'),
		        'task_time'       =>  date("Y-m-d H:i:s"),
		        'task'            =>  'Sent this patient with ID ' ." " . $patient_id . " ".'To The Laboratory'
		         ); 
		    	//insert Activities
		    	$this->DoctorModel->addLog($data);

		    	

				$this->session->set_flashdata('success', 'Forwarded to Laboratory Scientist This  is a unique ID');

				//redirect to the doctor to the forwardPatient Function for onward processing
				redirect(base_url() . "doctor/Dashboard/forwardPatient/" . $patient_id, 'refresh');
				//redirect('doctor/Dashboard/forwardPatient/'. $patient_id, 'refresh');

		  }else{//if it exist it updates

             
		  	$patient_id          	= $this->input->post('patient_id');
            $doctor_id          	= $this->session->userdata('pfNo');
            $patient_sname          = $this->input->post('patient_sname');
            $patient_fname          = $this->input->post('patient_fname');
            $patient_gender         = $this->input->post('patient_gender');
            $dob          			= $this->input->post('dob');
            $specimen_type          = $this->input->post('specimen_type');
            $prognosis              = $this->input->post('prognosis');
            $required_invest        = $this->input->post('required_invest');
            $submitted_date         = date("Y-m-d-H:i:s");
            $summary                = $this->input->post('summary');
		  	  
		  	  //data array
			$data['patient_id']  		= $patient_id;
			$data['doctor_id']  		= $doctor_id;
			$data['surname']  			= $patient_sname;
			$data['other_names']  		= $patient_fname;
			$data['gender']  			= $patient_gender;
			$data['dob']  				= $dob;
			$data['specimen_type']  	= $specimen_type;
			$data['prognosis']  		= $prognosis;
			$data['required_invest']  	= $required_invest;
			$data['submitted_date']  	= $submitted_date;
			$data['summary']  			= $summary;
			$data['investigated']  		= 0;

			$this->DoctorModel->submitGenLabRequestUpdate($data, $patient_id);

					//Activities log  array
		    	$data = array(
		        'pfNo'            =>  $this->session->userdata('pfNo'),
		        'email'           =>  $this->session->userdata('email'),
		        'task_time'       =>  date("Y-m-d H:i:s"),
		        'task'            =>  'Sent this patient with ID ' ." " . $patient_id . " ".'To The Laboratory'
		         ); 
		    	//insert Activities
		    	$this->DoctorModel->addLog($data);

		    	

				$this->session->set_flashdata('success', 'Forwarded to La Scientist');

				//redirect to the doctor to the forwardPatient Function for onward processing
				redirect(base_url() . "doctor/Dashboard/forwardPatient/" . $patient_id, 'refresh');
				//redirect('doctor/Dashboard/forwardPatient/'. $patient_id, 'refresh');
			

		}	
	}

	//This function submits what the doctor want to send to the 
	public function microFunction(){
		//setting validation rules
		$this->form_validation->set_rules('patient_id', 'patient ID','trim|required');
		$this->form_validation->set_rules('sname', 'Surname', 'trim|required');
		$this->form_validation->set_rules('oname', 'Other names', 'trim|required');
		$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
		$this->form_validation->set_rules('prognosis', 'Prognosis', 'trim|required');
		$this->form_validation->set_rules('specimen_required', 'Specimen Required', 'trim|required');
		$this->form_validation->set_rules('date_investigated', 'Date Investigated', 'trim|required');
		$this->form_validation->set_rules('doc_id', 'Doctor ID', 'trim|required');
		$id=$this->input->post('patient_id');

		//thsi query checks if thse patient is in the DB
		//if not it creates, it there is it updaes
		$this->db->select('*');
        $this->db->from('microbiology');
        $this->db->where('microbiology.patient_id', $id);
        $query = $this->db->get();
        $exist = $query->num_rows();

		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->microform($id);

		  }elseif(!$exist) { //if not exist is creates

		  	 //else is validation is true then create post array
            $patient_id          = $this->input->post('patient_id');
            $sname          	 = $this->input->post('sname');
            $oname          	 = $this->input->post('oname');
            $gender          	 = $this->input->post('gender');
            $dob          	 	 = $this->input->post('dob');
            $prognosis           = $this->input->post('prognosis');
            $specimen_required 	 = $this->input->post('specimen_required');
            $date_investigated 	 = $this->input->post('date_investigated');
            $doc_id          	 = $this->input->post('doc_id');

                

            $data['patient_id']  		= $patient_id;
            $data['sname']  			= $sname;
			$data['oname']  			= $oname;
			$data['gender']  			= $gender;
			$data['dob']  				= $dob;
			$data['prognosis']  		= $prognosis;
			$data['specimen_required']  = $specimen_required;
			$data['date_investigated']  = $date_investigated;
			$data['doc_id']  			= $doc_id;
 			$data['investigated']  		= 0;
		$this->DoctorModel->submitMicroBiologyInsert($data);

			//Activities log  array
    	$data = array(
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Sent this patient with ID ' ." " . $patient_id . " ".'To the Microbiologist'
         ); 
    	//insert Activities
    	$this->DoctorModel->addLog($data);

    	

		$this->session->set_flashdata('success', 'Forwarded to Microbiologist successfully');

		//redirect to the doctor to the forwardPatient Function for onward processing
		redirect(base_url() . "doctor/Dashboard/forwardPatient/" . $patient_id, 'refresh');
		//redirect('doctor/Dashboard/forwardPatient/'. $patient_id, 'refresh');

		  } else{

             

			//else is validation is true then create post array
            $patient_id          = $this->input->post('patient_id');
            $sname          	 = $this->input->post('sname');
            $oname          	 = $this->input->post('oname');
            $gender          	 = $this->input->post('gender');
            $dob          	 	 = $this->input->post('dob');
            $prognosis           = $this->input->post('prognosis');
            $specimen_required 	 = $this->input->post('specimen_required');
            $date_investigated 	 = $this->input->post('date_investigated');
            $doc_id          	 = $this->input->post('doc_id');

                

            $data['patient_id']  		= $patient_id;
            $data['sname']  			= $sname;
			$data['oname']  			= $oname;
			$data['gender']  			= $gender;
			$data['dob']  				= $dob;
			$data['prognosis']  		= $prognosis;
			$data['specimen_required']  = $specimen_required;
			$data['date_investigated']  = $date_investigated;
			$data['doc_id']  			= $doc_id;
			$data['investigated']  		= 0;
 
		$this->DoctorModel->submitMicroBiologyUpdate($data, $patient_id);

			//Activities log  array
    	$data = array(
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Sent this patient with ID ' ." " . $patient_id . " ".'To the Microbiologist'
         ); 
    	//insert Activities
    	$this->DoctorModel->addLog($data);

    	

		$this->session->set_flashdata('success', 'Forwarded to Microbiologist successfully');

		//redirect to the doctor to the forwardPatient Function for onward processing
		redirect(base_url() . "doctor/Dashboard/forwardPatient/" . $patient_id, 'refresh');
		//redirect('doctor/Dashboard/forwardPatient/'. $patient_id, 'refresh');

	}	
}

//This function selects the History of the patient whose ID is given 
public function ViewPatientHistory($patient_id){
	$data['view_patient'] = $this->DoctorModel->viewHistorySinglePatient($patient_id);
	$this->template->load('doctor', 'default','viewpatienthistory', $data);

	}

	//this function loads patient sent to the microbiologist
	public function viewMicroBiologist(){
		$data['view_patient'] = $this->DoctorModel->viewMicroBiologist();
		$this->template->load('doctor', 'default','viewmicrobiologist', $data);

	}

	//this function loads patient sent to the General Lab 
	public function viewGenLabReport(){
	$doc_id = $this->session->userdata('pfNo');
	$data['view_patient'] = $this->DoctorModel->viewGenLabReport($doc_id);
	$this->template->load('doctor', 'default','viewgenlabreport', $data);

	}

	//this function load the Microbilogist form for doctor to view report
	public function viewMicrobiologistReport($patient_id, $date_investigated){
	$data['view_patient'] = $this->DoctorModel->viewMicrobiologistReport($patient_id, $date_investigated);
	$this->template->load('doctor', 'default','viewmicrobiologistreport', $data);
	}

	//this function load the General Report Initially Submitted by the Doctor where they will be a link for the report back
	public function viewGenLabReportDoctor($patient_id, $submitted_date){

	$data['view_patient'] = $this->DoctorModel->viewGenLabReportDoctor($patient_id, $submitted_date);
	$this->template->load('doctor', 'default','viewgenlabreportdoctor', $data);

	}


	//this function loads the report from the lab
	public function viewPatientLabReport($patient_id){
	$data['patientdata'] = $this->DoctorModel->viewPatientLabReport($patient_id);
	$this->template->load('doctor', 'default','viewpatientlabreport', $data);
	}

	//this function loads patients whose diagnosis did not go through 
	public function failedDiagnosis(){
	$pfNo = $this->session->userdata('pfNo');
	$data['view_patient'] = $this->DoctorModel->failedDiagnosis($pfNo);
	$this->template->load('doctor', 'default','faileddiagnosis', $data);

	}

}