<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_controller extends CI_Controller {

	//This function load the view for the welcome page i.e the first page when you enter the URL
	public function index()
	{
		$this->load->view('welcome');
	}
}
