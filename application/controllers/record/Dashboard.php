<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	//This function load the view for the welcome page i.e the first page when you enter the URL
	public function index()
	{
		//Passing The State to the createstaff view  
      	
		$this->template->load('record', 'default','homeview');
		

	}

	//this funtion loads the view for registering patients by the records officer
	public function registerPatient(){
		//Passing The State to the createstaff view  
      	$data['state'] = $this->RecordModel->getState(); 
		$this->template->load('record', 'default','registerpatient', $data);

	}

	//this function perform the registration of patient and submit to database
	public function submitPatientRecord(){

		//setting validation rules
		$this->form_validation->set_rules('sname', 'Patient Surname','trim|required');
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
		$this->form_validation->set_rules('gender', 'Patient Gender', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[11]|max_length[11]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|is_unique[patients_table.email]');
		$this->form_validation->set_rules('category', 'Category', 'trim|required');
		$this->form_validation->set_rules('patient_id', 'Patient ID', 'trim');
		$this->form_validation->set_rules('state', 'Patient state', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('nok_fullname', 'Next of Kin Fullname', 'trim|required');
		$this->form_validation->set_rules('nok_phone', 'Next of Kin Phone Number', 'trim|required|min_length[11]|max_length[11]');
		
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->registerPatient();

		  }else{

		  	$birthdate			= $this->input->post('dob');
		  	$today        		= date("Y-m-d");

		  	$date1 = $birthdate;
			$date2 = $today;
			$diff = abs(strtotime($date1) - strtotime($date2));
			$years = floor($diff / (365*60*60*24));
		  	
		  	
			//else is validation is true then create post array
            $sname         			= $this->input->post('sname');
            $fname          		= $this->input->post('fname');
            $dob          			= $this->input->post('dob');
            $age 					= $years;
            $gender          		= $this->input->post('gender');
            $phone           		= $this->input->post('phone');
            $email         			= $this->input->post('email');
            $category         		= $this->input->post('category');
            $patient_id         	= $this->input->post('patient_id');
            $state         			= $this->input->post('state');
            $city					= $this->input->post('city');
            $nok_fullname 			= $this->input->post('nok_fullname');
            $nok_phone 				= $this->input->post('nok_phone');
            $reg_date 				= date("Y-m-d H:i:s");
            $active 				= 1;
                   
            
                 
            	if($category == 'Others'){
            	$othersID = "CL".rand(10, 10000)."FUT";

            	$data['sname'] 						= $sname;
				$data['fname']						= $fname;
				$data['dob']						= $dob;
				$data['age']						= $age;
				$data['gender']						= $gender;
				$data['phone']						= $phone;
				$data['email']						= $email;
				$data['category']					= $category;
				$data['patient_id']					= $othersID;
				$data['state']						= $state;
				$data['city']						= $city;
				$data['nok_fullname']				= $nok_fullname;
				$data['nok_phone']					= $nok_phone;
				$data['reg_date']					= $reg_date;
				$data['active']						= $active;



						//Register Staff (i.e Staff) by calling the function in the model class 
				$this->RecordModel->registerPatient($data);

					//Activities log  array
		    	$data = array(
		    
		        'pfNo'            =>  $this->session->userdata('pfNo'),
		        'email'           =>  $this->session->userdata('email'),
		        'task_time'       =>  date("Y-m-d H:i:s"),
		        'task'            =>  'created the record of a patient with patient_id' ." "  . $othersID
		         ); 
		    	//insert Activities
		    	$this->RecordModel->addLog($data);

            	}else{

            	$data['sname'] 						= $sname;
				$data['fname']						= $fname;
				$data['dob']						= $dob;
				$data['age']						= $age;
				$data['gender']						= $gender;
				$data['phone']						= $phone;
				$data['email']						= $email;
				$data['category']					= $category;
				$data['patient_id']					= $patient_id;
				$data['state']						= $state;
				$data['city']						= $city;
				$data['nok_fullname']				= $nok_fullname;
				$data['nok_phone']					= $nok_phone;
				$data['reg_date']					= $reg_date;
				$data['active']						= $active;


						//Register Staff (i.e Staff) by calling the function in the model class 
				$this->RecordModel->registerPatient($data);

					//Activities log  array
		    	$data = array(
		    
		        'pfNo'            =>  $this->session->userdata('pfNo'),
		        'email'           =>  $this->session->userdata('email'),
		        'task_time'       =>  date("Y-m-d H:i:s"),
		        'task'            =>  'created the record of a patient with patient_id' ." " . $patient_id
		         ); 
		    	//insert Activities
		    	$this->RecordModel->addLog($data);

            	}
                
				
			


		$this->session->set_flashdata('success', 'Patient registered successfully');

		$id = $this->db->insert_id();
		

			
		//redirect to the registration page and refreshes all form input
		redirect('record/Dashboard/registerPatient', 'refresh');

		}	

	}

	

	//this function extracts all registered patients in the DB
	public function viewPatients(){

		$data['view_patient'] = $this->RecordModel->getPatient();
		$this->template->load('record', 'default','viewpatients', $data);
	}

	//this function helps view appointment status of patients
	public function appointmentStatus(){
		$data['view_apt'] = $this->RecordModel->getAppointment();
		$this->template->load('record', 'default','appointmentstatus', $data);
	}

	//this function helps verify patient 
	public function verifyPatient(){
		$data['view_patient'] = $this->RecordModel->getPatient();
		$this->template->load('record', 'default','verifypatient', $data);
	}




	//function to get city for a particular state
    public function buildDrpCity(){
        //set selected stateid from POST  
      echo $stateid = $this->input->post('id',TRUE);
        //run the query for the city as we specified earlier
        //to get the city as specified in the model based on the stateid passed to the model
      $cityData['city']=$this->AdminModel->getCityByState($stateid);  
      $output = null;  
      foreach ($cityData['city'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
          $output .= "<option value='".$row->id."'>".$row->lganame."</option>";  
      }  
      echo $output;  
                                    
    }


    //this function to delete patient
	public function deletePatient($patient_id){
		
		$this->RecordModel->deletePatient($patient_id);

		//Activities log  array
    	$data = array(
    
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'deleted the record of patient with patient_id' ." " . $patient_id
         ); 
    	//insert Activities
    	$this->RecordModel->addLog($data);

		$this->session->set_flashdata('success', 'Record Deleted Succesfully');
        redirect(base_url() . "record/Dashboard/viewPatients", 'refresh');

	}

	//this function loads the view for us to edit a patient
	public function editPatient($patient_id){
		$data['state'] = $this->RecordModel->getState(); 
		$data['item'] = $this->RecordModel->getPatientByID($patient_id);
		$this->template->load('record', 'default','editpatient', $data);	
	}


//this function perform the update function for patient data
	public function updatePatient($patientID){

		//setting validation rules
		$this->form_validation->set_rules('sname', 'Patient Surname','trim|required');
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
		$this->form_validation->set_rules('gender', 'Patient Gender', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[11]|max_length[11]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required');
		$this->form_validation->set_rules('category', 'Category', 'trim|required');
		$this->form_validation->set_rules('patient_id', 'Patient ID', 'trim');
		$this->form_validation->set_rules('state', 'Patient state', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('nok_fullname', 'Next of Kin Fullname', 'trim|required');
		$this->form_validation->set_rules('nok_phone', 'Next of Kin Phone Number', 'trim|required|min_length[11]|max_length[11]');
		
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->editPatient($patientID);

		  }else{

		  	$birthdate			= $this->input->post('dob');
		  	$today        		= date("Y-m-d");

		  	$date1 = $birthdate;
			$date2 = $today;
			$diff = abs(strtotime($date1) - strtotime($date2));
			$years = floor($diff / (365*60*60*24));
		  	
		  	
            

			//else is validation is true then create post array
            $sname         			= $this->input->post('sname');
            $fname          		= $this->input->post('fname');
            $dob          			= $this->input->post('dob');
            $age 					= $years;
            $gender          		= $this->input->post('gender');
            $phone           		= $this->input->post('phone');
            $email         			= $this->input->post('email');
            $category         		= $this->input->post('category');
            $patient_id         	= $this->input->post('patient_id');
            $state         			= $this->input->post('state');
            $city					= $this->input->post('city');
            $nok_fullname 			= $this->input->post('nok_fullname');
            $nok_phone 				= $this->input->post('nok_phone');
            //$reg_date 			= $this->input->post('nok_phone');
            $active 				= 1;
                   
            
                 
            
            	

            	$data['sname'] 						= $sname;
				$data['fname']						= $fname;
				$data['dob']						= $dob;
				$data['age']						= $age;
				$data['gender']						= $gender;
				$data['phone']						= $phone;
				$data['email']						= $email;
				$data['category']					= $category;
				$data['patient_id']					= $patient_id;
				$data['state']						= $state;
				$data['city']						= $city;
				$data['nok_fullname']				= $nok_fullname;
				$data['nok_phone']					= $nok_phone;
				//$data['reg_date']					= $reg_date;
				$data['active']						= $active;

            	
                
				
			


		//update a patient record (i.e Patients) by calling the function in the model class 
		$this->RecordModel->updatePatient($patientID, $data);

			//Activities log  array
    	$data = array(
    
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'updated the record of a patient with patient_id' ." " . $patient_id
         ); 
    	//insert Activities
    	$this->RecordModel->addLog($data);


		$this->session->set_flashdata('success', 'Patient updated successfully');

		$id = $this->db->insert_id();
		
	
		//redirect to the view Patients function page and refreshes all form input
		redirect('record/Dashboard/viewPatients', 'refresh');

		}	

	}

	//THIS FUNCTION WILL LOAD THE TEMPLATE FOR BOOKING A PATIENT
	public function bookPatient($patient_id){
		$data['apptdatadoc'] = $this->RecordModel->getApptDataDoc(); 
		$data['apptdata'] = $this->RecordModel->getApptData($patient_id); 
		$this->template->load('record', 'default','bookpatient', $data);
		
	}

	public function submitPatientAppointment(){

		//setting validation rules
		$this->form_validation->set_rules('pf_no', 'Staff PF Number','trim|required');
		$this->form_validation->set_rules('patient_id', 'Patient ID', 'trim|required');
		$this->form_validation->set_rules('appt_date', 'Appointment Date', 'trim|required');
		$this->form_validation->set_rules('patient_name', 'Patient Name', 'trim|required');
		
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->verifyPatient();
			$this->session->set_flashdata('error', 'Appointment Booking Not Succesfull');

		  }else{

             

			//else is validation is true then create post array
            $pf_no         				= $this->input->post('pf_no');
            $patient_id          		= $this->input->post('patient_id');
            $appt_date          		= $this->input->post('appt_date');
            $patient_name          		= $this->input->post('patient_name');
            

                $data['pf_no'] 			= $pf_no;
				$data['patient_id']		= $patient_id;
				$data['appt_date']		= $appt_date;
				$data['patient_name']	= $patient_name;
                $data['diagnosed']	    = 0;
                
				
		$this->RecordModel->bookPatient($data);

			//Activities log  array
    	$data = array(
    
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Booked an appointment with a doctor with PF NO' . $pf_no . " " . "and a patientient with patient ID" . " ". $patient_id 
         ); 
    	//insert Activities
    	$this->RecordModel->addLog($data);


		$this->session->set_flashdata('success', 'Appointment successfully Booked');


		redirect('record/Dashboard/verifyPatient', 'refresh');

		}	


	}


}