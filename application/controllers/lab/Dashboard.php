<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	//This function load the view for the welcome page i.e the first page when you enter the URL
	public function index()
	{
				
		$this->template->load('lab', 'default','homeview');
	}

	public function managePatient()
	{
		$this->template->load('lab', 'default','managepatient');
	}

	//This function loads General Lab patients
	public function viewGeneralLab(){

		$data['view_patient'] = $this->LabModel->viewGeneralLabPatients();
		$this->template->load('lab', 'default','viewgenerallabpatients', $data);

	}

	//This function loads Microbiologist patients
	public function viewMicrobiologistPatient(){		
		$data['view_patient'] = $this->LabModel->viewMicrobiologistPatient();
		$this->template->load('lab', 'default','viewmicrobiologistpatient', $data);

	}

	//this function loads Microbiologist form 
	public function loadMicrobiologistForm($patient_id){
		$data['patientdata'] = $this->LabModel->loadMicrobiologistForm($patient_id);
		$this->template->load('lab', 'default','loadmicrobiologistform', $data);
	}

	//this function submits submits the microbiologist diagnosis
	public function microFunction(){

		//setting validation rules
		$this->form_validation->set_rules('phy_char', 'Physical Characteristics','trim|required');
		$this->form_validation->set_rules('chem_char', 'Chemical Characteristics', 'trim|required');
		$this->form_validation->set_rules('microscope', 'Microscope', 'trim|required');
		$this->form_validation->set_rules('culture_yield', 'Culture', 'trim|required');
		$this->form_validation->set_rules('staff_id', 'Staff ID', 'trim|required');
		$this->form_validation->set_rules('summary', 'Summary', 'trim|required');
		$id=$this->input->post('patient_id');
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->loadMicrobiologistForm($id);

		  }else{

             

			//else is validation is true then create post array
            $phy_char          				= $this->input->post('phy_char');
            $chem_char          			= $this->input->post('chem_char');
            $microscope 					= $this->input->post('microscope');
            $culture_yield 					= $this->input->post('culture_yield');
            $staff_id 						= $this->input->post('staff_id');
            $summary 						= $this->input->post('summary');

            $data['phy_char']  				= $phy_char;
            $data['chem_char']  			= $chem_char;
            $data['microscope']  			= $microscope;
            $data['culture_yield']  		= $culture_yield;
            $data['staff_id']  				= $staff_id;
            $data['summary']  				= $summary;
            $data['investigated']  			= 1;

			$this->LabModel->submitMicroBiologyUpdate($data, $id);

	
			//Activities log  array
    	$data = array(
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Investigated patient with ID ' ." " . $id . " ".'For View by Doctor.'
         ); 
    	//insert Activities
    	$this->LabModel->addLog($data);

    	

		$this->session->set_flashdata('success', 'Patient Successfully Investigated');

		
		redirect(base_url() . "lab/Dashboard/viewMicrobiologistPatient", 'refresh');
		

		}	


	}

	//this function loads the General lab request for a patient
	public function labRequest($patient_id){
		$data['patientdata'] = $this->LabModel->loadPatientGenLabForm($patient_id);
		$this->template->load('lab', 'default','labrequest', $data);
	}

	//this function loads the report form
	public function loadReportForm($patient_id, $doc_id){
		$data['docdata'] = $this->LabModel->fetcDocData($doc_id);
		$data['patientdata'] = $this->LabModel->loadPatientReportForm($patient_id);
		$this->template->load('lab', 'default','loadreportform', $data);

	}

	//this function submits the lab report by the Lab scientist
	public function submitLabReport(){


		//setting validation rules
		$this->form_validation->set_rules('patient_id', 'patient_id','trim|required');
		$this->form_validation->set_rules('report_date', 'Report Date', 'trim|required');
		$this->form_validation->set_rules('surname', 'Surname', 'trim|required');
		$this->form_validation->set_rules('othername', 'Other name', 'trim|required');
		$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
		$this->form_validation->set_rules('date_prognosis', 'Date of Prognosis', 'trim');
		$this->form_validation->set_rules('prognosis', 'Prognosis', 'trim');
		$this->form_validation->set_rules('specimen_type', 'Specimen Type', 'trim');
		$this->form_validation->set_rules('specimen_date', 'Specimen Date', 'trim');
		$this->form_validation->set_rules('doctor_name', 'Doctor Name', 'trim|required');
		$this->form_validation->set_rules('doctor_id', 'Doctor ID', 'trim|required');
		$this->form_validation->set_rules('HB', 'HB', 'trim');
		$this->form_validation->set_rules('H_B', 'HB (13 -18)', 'trim');
		$this->form_validation->set_rules('PVC', 'PVC (40-45)', 'trim');
		$this->form_validation->set_rules('WBC', 'WBC (4.8-10.8)', 'trim');
		$this->form_validation->set_rules('RBCC', 'RBC (4.5-5.5)', 'trim');
		$this->form_validation->set_rules('MCV', 'MCV (76-93)', 'trim');
		$this->form_validation->set_rules('MCH', 'MCH (27-31)', 'trim');
		$this->form_validation->set_rules('MCHC', 'MCHC (31-35)', 'trim');
		$this->form_validation->set_rules('ESR', 'ESR (WESTER GREEN)', 'trim');
		$this->form_validation->set_rules('DIFF', 'DIFF.N,L,M,E,B', 'trim');
		$this->form_validation->set_rules('RETICS_PER', 'RETICS (0-2%)', 'trim');
		$this->form_validation->set_rules('BLOOD_FILM', 'BLOOD_FILM', 'trim');
		$this->form_validation->set_rules('HB_GENOTYPE', 'HB_GENOTYPE', 'trim');
		$this->form_validation->set_rules('SICKING', 'SICKING', 'trim');
		$this->form_validation->set_rules('BLEED', 'BLEED', 'trim');
		$this->form_validation->set_rules('CLOTT', 'CLOTT', 'trim');
		$this->form_validation->set_rules('PLATELET_COUNT', 'PLATELET_COUNT', 'trim');
		$this->form_validation->set_rules('BLOODGROUP', 'BLOODGROUP', 'trim');
		$this->form_validation->set_rules('VDRL_TEST', 'VDRL_TEST', 'trim');
		$this->form_validation->set_rules('TPHA', 'TPHA', 'trim');
		$this->form_validation->set_rules('PREG_TEST', 'PREG_TEST', 'trim');
		$this->form_validation->set_rules('HIV', 'HIV', 'trim');
		$this->form_validation->set_rules('HBS_ag', 'HBS_ag', 'trim');
		$this->form_validation->set_rules('HCV', 'HCV', 'trim');
		$this->form_validation->set_rules('urine', 'urine', 'trim');
		$this->form_validation->set_rules('microscopy', 'microscopy', 'trim');
		$this->form_validation->set_rules('P', 'P', 'trim');
		$this->form_validation->set_rules('PROTEIN', 'PROTEIN', 'trim');
		$this->form_validation->set_rules('BLOOD', 'BLOOD', 'trim');
		$this->form_validation->set_rules('BILIYUBINE', 'BILIYUBINE', 'trim');
		$this->form_validation->set_rules('UROBIINOGEN', 'UROBIINOGEN', 'trim');
		$this->form_validation->set_rules('ASCOBIC_ACID', 'ASCOBIC_ACID', 'trim');
		$this->form_validation->set_rules('NITRATE', 'NITRATE', 'trim');
		$this->form_validation->set_rules('SP_GRAVITY', 'SP_GRAVITY', 'trim');
		$this->form_validation->set_rules('EPITHELIAL_CELL', 'EPITHELIAL_CELL', 'trim');
		$this->form_validation->set_rules('RBC', 'RBC', 'trim');
		$this->form_validation->set_rules('YEAST_CELLS', 'YEAST_CELLS', 'trim');
		$this->form_validation->set_rules('CRYSTALS', 'CRYSTALS', 'trim');
		$this->form_validation->set_rules('CASTS', 'CASTS', 'trim');
		$this->form_validation->set_rules('OVA', 'OVA', 'trim');
		$this->form_validation->set_rules('TRICHOMONAS_VAGINALIS', 'TRICHOMONAS_VAGINALIS', 'trim');
		$this->form_validation->set_rules('OTHERS', 'OTHERS', 'trim');
		$this->form_validation->set_rules('SOMATIC_ANTIGEN_O', 'SOMATIC_ANTIGEN_O', 'trim');
		$this->form_validation->set_rules('FLAGELLA_ANTIGEN_H', 'FLAGELLA_ANTIGEN_H', 'trim');
		$this->form_validation->set_rules('PARATYPHI_A', 'PARATYPHI_A', 'trim');
		$this->form_validation->set_rules('PARATYPHIa', 'PARATYPHIa', 'trim');
		$this->form_validation->set_rules('PARATYPHI_B', 'PARATYPHI_B', 'trim');
		$this->form_validation->set_rules('PARATYPHIb', 'PARATYPHIb', 'trim');
		$this->form_validation->set_rules('PARATYPHI_C', 'PARATYPHI_C', 'trim');
		$this->form_validation->set_rules('PARATYPHIc', 'PARATYPHIc', 'trim');
		$this->form_validation->set_rules('PARATYPHI_D', 'PARATYPHI_D', 'trim');
		$this->form_validation->set_rules('PARATYPHId', 'PARATYPHI_D', 'trim');
		$this->form_validation->set_rules('summary', 'summary', 'trim');
		$this->form_validation->set_rules('summary', 'summary', 'trim');

		
		
		$patient_id=$this->input->post('patient_id');
		$d_id=$this->input->post('doctor_id');

		
		//thsi query checks if thse patient is in the DB
		//if not it creates, it there is it updaes
		$this->db->select('*');
        $this->db->from('lab_report_form');
        $this->db->where('patient_id', $patient_id);
        $query = $this->db->get();
        $exist = $query->num_rows();

		
		//form validation to check form inputs
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->loadReportForm($patient_id, $d_id);

		  }elseif(!$exist) { //if not exist is creates

		  	
            $data['patient_id']          	= $this->input->post('patient_id');
            $data['report_date']          	= $this->input->post('report_date');
            $data['surname']          		= $this->input->post('surname');
            $data['othername']          	= $this->input->post('othername');
            $data['gender']         	    = $this->input->post('gender');
            $data['date_prognosis']     	= $this->input->post('date_prognosis');
            $data['prognosis']     			= $this->input->post('prognosis');
            $data['specimen_type']     		= $this->input->post('specimen_type');
            $data['specimen_date']     		= $this->input->post('specimen_date');
            $data['doctor_name']     		= $this->input->post('doctor_name');
            $data['HB']     				= $this->input->post('HB');
            $data['H_B']   					= $this->input->post('H_B');
            $data['PVC']     				= $this->input->post('PVC');
            $data['WBC']     			    = $this->input->post('WBC');
            $data['RBCC']     				= $this->input->post('RBCC');
            $data['MCV']     				= $this->input->post('MCV');
            $data['MCH'] 					= $this->input->post('MCH');
            $data['MCHC']     				= $this->input->post('MCHC');
            $data['ESR'] 					= $this->input->post('ESR');
            $data['DIFF']					= $this->input->post('DIFF');
            $data['RETICS_PER']     		= $this->input->post('RETICS_PER');
            $data['BLOOD_FILM']     		= $this->input->post('BLOOD_FILM');
            $data['HB_GENOTYPE']     		= $this->input->post('HB_GENOTYPE');
            $data['SICKING']	     		= $this->input->post('SICKING');
            $data['BLEED']		     		= $this->input->post('BLEED');
            $data['CLOTT']		     		= $this->input->post('CLOTT');
            $data['PLATELET_COUNT']    		= $this->input->post('PLATELET_COUNT');
            $data['BLOODGROUP']    			= $this->input->post('BLOODGROUP');
            $data['VDRL_TEST']    			= $this->input->post('VDRL_TEST');
            $data['TPHA']    				= $this->input->post('TPHA');
            $data['PREG_TEST']    			= $this->input->post('PREG_TEST');
            $data['HIV']    				= $this->input->post('HIV');
            $data['HBS_ag']    				= $this->input->post('HBS_ag');
            $data['HCV']    				= $this->input->post('HCV');
            $data['urine']    				= $this->input->post('urine');
            $data['microscopy']    			= $this->input->post('microscopy');
            $data['P'] 			   			= $this->input->post('P');
            $data['PROTEIN'] 			   	= $this->input->post('PROTEIN');
            $data['BLOOD'] 			   		= $this->input->post('BLOOD');
            $data['BILIYUBINE'] 			= $this->input->post('BILIYUBINE');
            $data['UROBIINOGEN'] 			= $this->input->post('UROBIINOGEN');
            $data['ASCOBIC_ACID'] 			= $this->input->post('ASCOBIC_ACID');
            $data['NITRATE'] 				= $this->input->post('NITRATE');
            $data['SP_GRAVITY'] 			= $this->input->post('SP_GRAVITY');
            $data['EPITHELIAL_CELL'] 		= $this->input->post('EPITHELIAL_CELL');
            $data['RBC']			 		= $this->input->post('RBC');
            $data['YEAST_CELLS']			= $this->input->post('YEAST_CELLS');
            $data['CRYSTALS']				= $this->input->post('CRYSTALS');
            $data['CASTS']					= $this->input->post('CASTS');
            $data['OVA']					= $this->input->post('OVA');
            $data['TRICHOMONAS_VAGINALIS']	= $this->input->post('TRICHOMONAS_VAGINALIS');
            $data['OTHERS'] 				= $this->input->post('OTHERS');
            $data['SOMATIC_ANTIGEN_O']		= $this->input->post('SOMATIC_ANTIGEN_O');
            $data['FLAGELLA_ANTIGEN_H'] 	= $this->input->post('FLAGELLA_ANTIGEN_H');
            $data['PARATYPHI_A'] 			= $this->input->post('PARATYPHI_A');
            $data['PARATYPHIa'] 			= $this->input->post('PARATYPHIa');
            $data['PARATYPHI_B'] 			= $this->input->post('PARATYPHI_B');
            $data['PARATYPHIb'] 			= $this->input->post('PARATYPHIb');
            $data['PARATYPHI_C'] 			= $this->input->post('PARATYPHI_C');
            $data['PARATYPHIc'] 			= $this->input->post('PARATYPHIc');
            $data['PARATYPHI_D'] 			= $this->input->post('PARATYPHI_D');
            $data['PARATYPHId'] 			= $this->input->post('PARATYPHId');
            $data['summary'] 				= $this->input->post('summary');
            $data['doctor_id'] 				= $this->input->post('doctor_id');
            $data['labstaff_id'] 			= $this->session->userdata('pfNo');

         	//this send 1 to the general_lab_request_form table to indicate the patient have been tested
         	$patient_id          	= $this->input->post('patient_id');
            $done['investigated'] = 1;
            $this->LabModel->patientTested($done, $patient_id);

			$this->LabModel->submitGenLabRequestInsert($data);

					//Activities log  array
		    	$data = array(
		        'pfNo'            =>  $this->session->userdata('pfNo'),
		        'email'           =>  $this->session->userdata('email'),
		        'task_time'       =>  date("Y-m-d H:i:s"),
		        'task'            =>  'Submitted this patient with ID ' ." " . $patient_id . " ".'General Lab Result'
		         ); 
		    	//insert Activities
		    	$this->LabModel->addLog($data);

		    	

				$this->session->set_flashdata('success', 'Patient Succesfully Underwent Laboratory Examination');

				redirect(base_url() . "lab/Dashboard/viewGeneralLab", 'refresh');
				

		  }else{//if it exist it updates

             
		  	
            $data['patient_id']          	= $this->input->post('patient_id');
            $data['report_date']          	= $this->input->post('report_date');
            $data['surname']          		= $this->input->post('surname');
            $data['othername']          	= $this->input->post('othername');
            $data['gender']         	    = $this->input->post('gender');
            $data['date_prognosis']     	= $this->input->post('date_prognosis');
            $data['prognosis']     			= $this->input->post('prognosis');
            $data['specimen_type']     		= $this->input->post('specimen_type');
            $data['specimen_date']     		= $this->input->post('specimen_date');
            $data['doctor_name']     		= $this->input->post('doctor_name');
            $data['HB']     				= $this->input->post('HB');
            $data['H_B']   					= $this->input->post('H_B');
            $data['PVC']     				= $this->input->post('PVC');
            $data['WBC']     			    = $this->input->post('WBC');
            $data['RBCC']     				= $this->input->post('RBCC');
            $data['MCV']     				= $this->input->post('MCV');
            $data['MCH'] 					= $this->input->post('MCH');
            $data['MCHC']     				= $this->input->post('MCHC');
            $data['ESR'] 					= $this->input->post('ESR');
            $data['DIFF']					= $this->input->post('DIFF');
            $data['RETICS_PER']     		= $this->input->post('RETICS_PER');
            $data['BLOOD_FILM']     		= $this->input->post('BLOOD_FILM');
            $data['HB_GENOTYPE']     		= $this->input->post('HB_GENOTYPE');
            $data['SICKING']	     		= $this->input->post('SICKING');
            $data['BLEED']		     		= $this->input->post('BLEED');
            $data['CLOTT']		     		= $this->input->post('CLOTT');
            $data['PLATELET_COUNT']    		= $this->input->post('PLATELET_COUNT');
            $data['BLOODGROUP']    			= $this->input->post('BLOODGROUP');
            $data['VDRL_TEST']    			= $this->input->post('VDRL_TEST');
            $data['TPHA']    				= $this->input->post('TPHA');
            $data['PREG_TEST']    			= $this->input->post('PREG_TEST');
            $data['HIV']    				= $this->input->post('HIV');
            $data['HBS_ag']    				= $this->input->post('HBS_ag');
            $data['HCV']    				= $this->input->post('HCV');
            $data['urine']    				= $this->input->post('urine');
            $data['microscopy']    			= $this->input->post('microscopy');
            $data['P'] 			   			= $this->input->post('P');
            $data['PROTEIN'] 			   	= $this->input->post('PROTEIN');
            $data['BLOOD'] 			   		= $this->input->post('BLOOD');
            $data['BILIYUBINE'] 			= $this->input->post('BILIYUBINE');
            $data['UROBIINOGEN'] 			= $this->input->post('UROBIINOGEN');
            $data['ASCOBIC_ACID'] 			= $this->input->post('ASCOBIC_ACID');
            $data['NITRATE'] 				= $this->input->post('NITRATE');
            $data['SP_GRAVITY'] 			= $this->input->post('SP_GRAVITY');
            $data['EPITHELIAL_CELL'] 		= $this->input->post('EPITHELIAL_CELL');
            $data['RBC']			 		= $this->input->post('RBC');
            $data['YEAST_CELLS']			= $this->input->post('YEAST_CELLS');
            $data['CRYSTALS']				= $this->input->post('CRYSTALS');
            $data['CASTS']					= $this->input->post('CASTS');
            $data['OVA']					= $this->input->post('OVA');
            $data['TRICHOMONAS_VAGINALIS']	= $this->input->post('TRICHOMONAS_VAGINALIS');
            $data['OTHERS'] 				= $this->input->post('OTHERS');
            $data['SOMATIC_ANTIGEN_O']		= $this->input->post('SOMATIC_ANTIGEN_O');
            $data['FLAGELLA_ANTIGEN_H'] 	= $this->input->post('FLAGELLA_ANTIGEN_H');
            $data['PARATYPHI_A'] 			= $this->input->post('PARATYPHI_A');
            $data['PARATYPHIa'] 			= $this->input->post('PARATYPHIa');
            $data['PARATYPHI_B'] 			= $this->input->post('PARATYPHI_B');
            $data['PARATYPHIb'] 			= $this->input->post('PARATYPHIb');
            $data['PARATYPHI_C'] 			= $this->input->post('PARATYPHI_C');
            $data['PARATYPHIc'] 			= $this->input->post('PARATYPHIc');
            $data['PARATYPHI_D'] 			= $this->input->post('PARATYPHI_D');
            $data['PARATYPHId'] 			= $this->input->post('PARATYPHId');
            $data['summary'] 				= $this->input->post('summary');
            $data['doctor_id'] 				= $this->input->post('doctor_id');
            $data['labstaff_id'] 			= $this->session->userdata('pfNo');
            
            
         
            $patient_id          	= $this->input->post('patient_id');
			$this->LabModel->submitGenLabRequestUpdate($data,$patient_id);

			//this send 1 to the general_lab_request_form table to indicate the patient have been tested
            $done['investigated'] = 1;
            $this->LabModel->patientTested($done, $patient_id);


					//Activities log  array
		    	$data = array(
		        'pfNo'            =>  $this->session->userdata('pfNo'),
		        'email'           =>  $this->session->userdata('email'),
		        'task_time'       =>  date("Y-m-d H:i:s"),
		        'task'            =>  'Submitted this patient with ID ' ." " . $patient_id . " ".'General Lab Result'
		         ); 
		    	//insert Activities
		    	$this->LabModel->addLog($data);

		    	

				$this->session->set_flashdata('success', 'Patient Succesfully Underwent Laboratory Examination');

				redirect(base_url() . "lab/Dashboard/viewGeneralLab", 'refresh');	

		}	

	}
}




