<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	//This function load the view for the welcome page i.e the first page when you enter the URL
	public function index()
	{
		
		
		$this->template->load('pharm', 'default','homeview');
	}

	public function dispenseDrug()
	{
		$data['viewpatient'] = $this->PharmModel->viewPatient();
		$this->template->load('pharm', 'default','dispensedrug',$data);
	}

	//this function selects a single patient for nurse to administer medication
	public function administerPrescription($patient_id, $prescription_date){

		//$patient_id 		= $this->uri->segment(4); //this values came from the URI
		//$prescription_date  = $this->uri->segment(5); 
		
		$data['viewpatient'] = $this->PharmModel->pharmViewForTreatment($patient_id, $prescription_date);
		$this->template->load('pharm', 'default','administerprescription', $data);
	}

	//this function submits medication done by the pharmacist
	public function pharmFunction(){


		//setting validation rules
		$this->form_validation->set_rules('patient_id', 'patient_id','trim|required');
		$this->form_validation->set_rules('diagnosis', 'diagnosis', 'trim|required');
		$this->form_validation->set_rules('prescription_date', 'prescription_date', 'trim|required');
		$this->form_validation->set_rules('pharmacy_prescription', 'Prescription for Pharmacist', 'trim|required');

		$id 				=$this->input->post('patient_id');
		$prescription_date  = $this->input->post('prescription_date'); 
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->administerPrescription($id, $prescription_date);

		  }else{

             

			//else is validation is true then create post array
            $pharmacy_prescription          = $this->input->post('pharmacy_prescription');
            $patient_id          			= $this->input->post('patient_id');
            $pharm_id          				= $this->session->userdata('pfNo');
            $prescription_date				= $this->input->post('prescription_date');

            $data['drug_update']  			= $pharmacy_prescription;
            $data['pharm_id']  				= $pharm_id;
			$this->PharmModel->PharmDrugUpdate($data, $patient_id, $prescription_date);

			//this table add pharmacy prescription to the patient health history table
			//History table input
			$history['drug_update']  		   = $pharmacy_prescription;
			$history['pharm_id']  			   = $pharm_id;
			$this->PharmModel->HistoryDrugUpdate($history, $patient_id, $prescription_date);

			//Activities log  array
    	$data = array(
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Administered Medication to patient with ID ' ." " . $patient_id . " ".'as directed by the Doctor'
         ); 
    	//insert Activities
    	$this->PharmModel->addLog($data);

    	

		$this->session->set_flashdata('success', 'Medication successfully Administered');

		
		redirect(base_url() . "pharm/Dashboard/dispenseDrug", 'refresh');
		

		}	


	}
}




