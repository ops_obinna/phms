<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
    parent::__construct();

    //check Login
    if(!$this->session->userdata('logged_in')){
      redirect('Login_Controller/logout');
    }
  }

	//this function enables the admin to reset his account 
	public function resetAccount(){
		$this->template->load('admin', 'default','resetaccount');
	}

	//This function load the view for the welcome page i.e the first page when you enter the URL
	public function index()
	{
		$data['numOfStaff'] = $this->AdminModel->staffCount();
		$this->template->load('admin', 'default','homeview', $data);
	}

	//this function load the template form for collection of staff registration data 
	public function createStaff()
	{
		//Passing The State to the createstaff view  
      	$data['state'] = $this->AdminModel->getState(); 
		$this->template->load('admin', 'default','createstaff', $data);

	}
	//this function allows admin to view staff record for subsequent update and deleteion  staff record
	public function updateStaffRecord()
	{
		$data['view_staff'] = $this->AdminModel->getStaff();
		$this->template->load('admin', 'default','updatestaffrecord', $data);

	}
	//This function allows admin to view staff logs and view what Nurses, and othe staff member have been doing
	public function viewStaffLogs()
	{
		$data['userlog'] = $this->AdminModel->getUserLog();
		$this->template->load('admin', 'default','viewstaffogs', $data);
	}


	//This function is used to register a staff member
	public function registerStaff()
	{
		//setting validation rules
		$this->form_validation->set_rules('pfNo', 'Staff PF Number','trim|required|is_unique[staff.pfNo]');
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('sname', 'Surname', 'trim|required');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|is_unique[staff.email]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[11]|max_length[11]|is_unique[staff.phone]');
		$this->form_validation->set_rules('specialization', 'Specialization', 'trim|required');
		$this->form_validation->set_rules('doctorspecialization', 'Doctor Specialization', 'trim');
		
		if ($this->form_validation->run() == FALSE)
          {
	 		
			$this->createStaff();

		  }else{

             

			//else is validation is true then create post array
            $pfNo         			= $this->input->post('pfNo');
            $fname          		= $this->input->post('fname');
            $sname          		= $this->input->post('sname');
            $email          		= $this->input->post('email');
            $pass           		= $this->input->post('password');
            $passhashed     		= password_hash($pass, PASSWORD_DEFAULT);
            $gender         		= $this->input->post('gender');
            $city         			= $this->input->post('city');
            $state         			= $this->input->post('state');
            $regdate        		= date("Y-m-d H:i:s");
            $phone         			= $this->input->post('phone');
            $specialization			= $this->input->post('specialization');
            $doctorspecialization 	= $this->input->post('doctorspecialization');
                   
            
            //$img            = $_FILES["staff_img"]["name"];       

                $data['pfNo'] 						= $pfNo;
				$data['fname']						= $fname;
				$data['sname']						= $sname;
				$data['email']						= $email;
				$data['password']					= $passhashed;
				$data['gender']						= $gender;
				$data['city']						= $city;
				$data['state']						= $state;
				$data['regdate']					= $regdate;
				$data['phone']						= $phone;
				$data['specialization']				= $specialization;
				$data['doctorspecialization']		= $doctorspecialization;
				
			//$data['rep_img'] 		= $img;


		//Register Staff (i.e Staff) by calling the function in the model class 
		$this->AdminModel->registerStaff($data);

			//Activities log  array
    	$data = array(
    
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'created the record of clinic staff with PFNO' ." " . $pfNo
         ); 
    	//insert Activities
    	$this->AdminModel->addLog($data);


		$this->session->set_flashdata('success', 'Staff registered successfully');

		$id = $this->db->insert_id();
		//move_uploaded_file($_FILES["staff_img"]["tmp_name"], "upload/staff/" . $id . '.jpg');

			
		//redirect to the registration page and refrshes all form input
		redirect('admin/Dashboard/registerStaff', 'refresh');

		}	

	}


	//this function to editStaff loads the form for update actvitys
	public function editStaff($pfNo){
		$data['state'] = $this->AdminModel->getState(); 
		$data['item'] = $this->AdminModel->getStaffByID($pfNo);
		$this->template->load('admin', 'default','editstaff', $data);	
	}
	//this function to delete staff
	public function deleteStaff($pfNo){
		
		$this->AdminModel->deleteStaff($pfNo);

		//Activities log  array
    	$data = array(
    
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'deleted the record of clinic staff with PFNO' ." " . $pfNo
         ); 
    	//insert Activities
    	$this->AdminModel->addLog($data);

		$this->session->set_flashdata('success', 'Record Deleted Succesfully');
        redirect(base_url() . "admin/Dashboard/updateStaffRecord", 'refresh');

	}

	//this function perform the update activity for staff of the medical center
	public function updateStaff($pfNo){

		//setting validation rules
		$this->form_validation->set_rules('pfNo', 'Staff PF Number','trim|required');
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('sname', 'Surname', 'trim|required');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
		$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[11]|max_length[11]');
		$this->form_validation->set_rules('specialization', 'Specialization', 'trim|required');
		$this->form_validation->set_rules('doctorspecialization', 'Doctor Specialization', 'trim');
		
		if ($this->form_validation->run() == FALSE)
          {
          	//get state data from the db and pass to view
          	$data['state'] = $this->AdminModel->getState(); 
          	//Get the present vales for the currently edited Id
          	$data['item'] = $this->AdminModel->getStaffByID($pfNo);
	 		//load template if validation is false 
			$this->template->load('admin', 'default','editStaff', $data);

		  }else{
  
			//else is validation is true then create post array
			
            $data['pfNo']          			= $this->input->post('pfNo');
            $data['fname']          		= $this->input->post('fname');
            $data['sname']          		= $this->input->post('sname');
            $data['email']         			= $this->input->post('email');
            $data['gender']        			= $this->input->post('gender');
            $data['city']         			= $this->input->post('city');
            $data['state']         			= $this->input->post('state');
            $data['phone']         			= $this->input->post('phone');
            $data['specialization']			= $this->input->post('specialization');
            $data['doctorspecialization'] 	= $this->input->post('doctorspecialization');
                
				
		//Register Staff (i.e Staff) by calling the function in the model class 
		$this->AdminModel->updateStaffByID($pfNo, $data);


		//Activities log  arrayf
    	$data = array(
    
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'updated the record of clinic staff with PFNO' ." " . $data['pfNo']
         ); 
    	//insert Activities
    	$this->AdminModel->addLog($data);

		$this->session->set_flashdata('success', 'Staff record updated successfully');

		//$id = $this->db->insert_id();
		//move_uploaded_file($_FILES["staff_img"]["tmp_name"], "upload/staff/" . $id . '.jpg');

			
		//redirect to the registration page and refrshes all form input
		redirect('admin/Dashboard/updateStaffRecord', 'refresh');

		}	

	}


	//Change the admin password
    public function updateAdminPassword(){
        
        /*Form Validation Rules 4 PROFILE UPDATE */

        $this->form_validation->set_rules('password', 'Current Password', 'trim|required');
        $this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('confirm_new_password', 'Confirm New Password', 'trim|required|matches[new_password]');
        
        
         if ($this->form_validation->run() == FALSE) {
                
                
                   
            $data['admin'] = $this->db->get_where('admin', array(
                        'id' => $this->session->userdata('id')
                    ))->result_array();
            //load template if validation is false 
                
             
			 $this->template->load('admin', 'default', 'manageadmin');
                
            }
        else{
            
        $data['password'] = $this->input->post('password');
        $data['new_password'] = $this->input->post('new_password');
        $data['confirm_new_password'] = $this->input->post('confirm_new_password');
            
        //Fetch Current Password from database admin_table
        $current_password = $this->db->get_where('admin', array('id' => $this->session->userdata('id')
                ))->row()->password;
            
            
        //compare the current password with the password entered current password  if it matches forward to table in database  
        if (password_verify($data['password'], $current_password)) {
            $this->db->where('id', $this->session->userdata('id'));
            $this->db->update('admin', array(
            'password' => password_hash($data['new_password'], PASSWORD_DEFAULT)));

            $this->session->set_flashdata('success', 'password_updated');

            echo '<script>alert(\'Password Update Successfull\');</script>';

             //Activities log  array
			    $data = array(
			    
			        'pfNo'            =>  $this->session->userdata('pfNo'),
			        'email'           =>  $this->session->userdata('email'),
			        'task_time'       =>  date("Y-m-d H:i:s"),
			        'task'            =>  'Admin Changed its Login passsword'
			         ); 
			    //insert Activities
			    $this->AdminModel->addLog($data);

            	

            

            $this->session->sess_destroy();
    		redirect('Login_Controller', 'refresh');

            } else {
                    // Your Current paasword is incorrect
            echo '<script>alert(\'You have entered a wrong account passwod\');</script>';
                }
            
            redirect(base_url() . 'admin/Dashboard/updateAdminPassword' , 'refresh');
            
        }
            
             

    }

  
    //function to get city for a particular state
    public function buildDrpCity(){
        //set selected stateid from POST  
      echo $stateid = $this->input->post('id',TRUE);
        //run the query for the city as we specified earlier
        //to get the city as specified in the model based on the stateid passed to the model
      $cityData['city']=$this->AdminModel->getCityByState($stateid);  
      $output = null;  
      foreach ($cityData['city'] as $row)  
      {  
         //here we build a dropdown item line for each  query result  
          $output .= "<option value='".$row->id."'>".$row->lganame."</option>";  
      }  
      echo $output;  
                                    
    }

}