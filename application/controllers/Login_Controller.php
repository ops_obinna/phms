<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_Controller extends CI_Controller {
    
    function __construct(){
    parent::__construct();

    
  }
     
  public function index()
  {
        //load login template
       $this->template->load('public', 'default', 'login');

  }

    
     //THIS FUNCTION TAKES CARE OF THE LOGIN FUNCTIONALITY OF THE SYSTEM USERS 
  /* ADMIN - DOCTORS - NURSES - LAB SCIENTIST - RECORD OFFICER - PHARMACIST */
      public function login_validation(){
    
            //Check Validation rules 
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            
            if($this->form_validation->run() == FALSE){ 
      
            $this->template->load('public', 'default', 'login');
                
            }else{
                  
                  //get post Data 
                  $email = $this->input->post('email');
                  $password = $this->input->post('password');
                  
                  //VARIABLES TO GET LOGIN MODEL FUNCTIONS FOR DIFFERENT USERS
                  $admin_id     = $this->LoginModel->loginAdmin($email);
                  $doc_id       = $this->LoginModel->loginDoc($email);
                  $nurse_id     = $this->LoginModel->loginNurse($email);
                  $lab_id       = $this->LoginModel->loginLab($email);
                  $rec_id       = $this->LoginModel->loginRecords($email);
                  $pharm_id     = $this->LoginModel->loginPharm($email);
                  
                 
                if($admin_id){
                  foreach($admin_id as $row){
                    
                       if (password_verify($password, $row->password)) {
                           
                        $admin_data  = array(
                              'id'               => $row->id,
                              'pfNo'             => $row->pfNo,
                              'email'            => $email,
                              'logged_in'        => true
                             
                               );

                        //set Session Data
                        $this->session->set_userdata($admin_data);

                        //Activities log  array
                        $data = array(
                            
                            'pfNo'              =>  $row->pfNo,
                            'email'             =>  $email,
                            'task_time'         =>  date("Y-m-d H:i:s"),
                            'task'              =>  'Logged In'
                            );
                          //insert Activities
                          $this->AdminModel->addLog($data);

            
                        //set Message 
                         $this->session->set_flashdata('success','Admin sucessfully  logged in');

                        //redirect
                        redirect('admin/dashboard','refresh');
                      

                  }else{
                           $this->session->set_flashdata('error','Wrong Password');
                            redirect('login_controller');
                       }
                  }
            }
            elseif($doc_id){
                  foreach($doc_id as $row){
                    
                       if (password_verify($password, $row->password)) {
                
                        $doc_data  = array(
                              'id'               => $row->id,
                              'pfNo'             => $row->pfNo,
                              'email'            => $email,
                              'fname'            => $row->fname,
                              'sname'            => $row->sname,
                              'logged_in'        => true
                             
                               );

                        //set Session Data
                        $this->session->set_userdata($doc_data);

                        //Activities log  array
                        $data = array(
                            
                            'pfNo'              =>  $row->pfNo,
                            'email'             =>  $email,
                            'task_time'         =>  date("Y-m-d H:i:s"),
                            'task'              =>  'Logged In'
                            );
                          //insert Activities
                          $this->AdminModel->addLog($data);

            
                        //set Message 
                         $this->session->set_flashdata('success','Doctor sucessfully  logged in');

                        //redirect
                        redirect('doctor/dashboard');
                  }else{
                           $this->session->set_flashdata('error','Wrong Password');
                            redirect('login_controller');
                       }
                  }
            } 
             elseif($nurse_id){
                  foreach($nurse_id as $row){
                    
                       if (password_verify($password, $row->password)) {
                
                        $nurse_data  = array(
                              'id'               => $row->id,
                              'pfNo'             => $row->pfNo,
                              'email'            => $email,
                              'logged_in'        => true
                             
                               );

                        //set Session Data
                        $this->session->set_userdata($nurse_data);

                        //Activities log  array
                        $data = array(
                            
                            'pfNo'              =>  $row->pfNo,
                            'email'             =>  $email,
                            'task_time'         =>  date("Y-m-d H:i:s"),
                            'task'              =>  'Logged In'
                            );
                          //insert Activities
                          $this->AdminModel->addLog($data);
            
                        //set Message 
                         $this->session->set_flashdata('success','Nurse sucessfully  logged in');

                        //redirect
                        redirect('nurse/dashboard');
                  }else{
                           $this->session->set_flashdata('error','Wrong Password');
                            redirect('login_controller');
                       }
                  }
            }   
             elseif($lab_id){
                  foreach($lab_id as $row){
                    
                       if (password_verify($password, $row->password)) {
                
                        $lab_data  = array(
                              'id'               => $row->id,
                              'pfNo'             => $row->pfNo,
                              'email'            => $email,
                              'logged_in'        => true
                             
                               );

                        //set Session Data
                        $this->session->set_userdata($lab_data);

                          //Activities log  array
                        $data = array(
                            
                            'pfNo'              =>  $row->pfNo,
                            'email'             =>  $email,
                            'task_time'         =>  date("Y-m-d H:i:s"),
                            'task'              =>  'Logged In'
                            );
                          //insert Activities
                          $this->AdminModel->addLog($data);

            
                        //set Message 
                         $this->session->set_flashdata('success','Lab Scientist sucessfully  logged in');

                        //redirect
                        redirect('lab/dashboard');
                  }else{
                           $this->session->set_flashdata('error','Wrong Password');
                            redirect('login_controller');
                       }
                  }
            }  
            elseif($pharm_id){
                  foreach($pharm_id as $row){
                    
                       if (password_verify($password, $row->password)) {
                
                        $pharm_data  = array(
                              'id'              => $row->id,
                              'pfNo'            => $row->pfNo,
                              'email'           => $email,
                              'logged_in'       => true
                             
                               );

                        //set Session Data
                        $this->session->set_userdata($pharm_data);

                        //Activities log  array
                        $data = array(
                            
                            'pfNo'              =>  $row->pfNo,
                            'email'             =>  $email,
                            'task_time'         =>  date("Y-m-d H:i:s"),
                            'task'              =>  'Logged In'
                            );
                          //insert Activities
                          $this->AdminModel->addLog($data);
            
                        //set Message 
                         $this->session->set_flashdata('success','Pharmacist sucessfully  logged in');

                        //redirect
                        redirect('pharm/dashboard');
                  }else{
                           $this->session->set_flashdata('error','Wrong Password');
                            redirect('login_controller');
                       }
                  }
            }  
            elseif($rec_id){
                  foreach($rec_id as $row){
                    
                       if (password_verify($password, $row->password)) {
                
                        $rec_data  = array(
                              'id'              => $row->id,
                              'pfNo'            => $row->pfNo,
                              'email'           => $email,
                              'logged_in'       => true
                             
                               );

                        //set Session Data
                        $this->session->set_userdata($rec_data);
                        
                          //Activities log  array
                        $data = array(
                            
                            'pfNo'              =>  $row->pfNo,
                            'email'             =>  $email,
                            'task_time'         =>  date("Y-m-d H:i:s"),
                            'task'              =>  'Logged In'
                            );
                          //insert Activities
                          $this->AdminModel->addLog($data);


                        //set Message 
                         $this->session->set_flashdata('success','Record Officer sucessfully  logged in');

                        //redirect
                        redirect('record/dashboard');
                  }else{
                           $this->session->set_flashdata('error','Wrong Password');
                            redirect('login_controller');
                       }
                  }
            }                       
            else{
                  //displays errors if password or email is wrong
                   $this->session->set_flashdata('error','Invalid Login');
                   redirect('login_controller');
                           
                }
        }
   }  //END FUNCTION LOGIN_VALIDATION
   
   //LOGOUT FUNCTION
   public function logout(){
    //Activities log  array
    $data = array(
    
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Logged out'
         ); 
    //insert Activities
    $this->AdminModel->addLog($data);

    $this->session->sess_destroy();
    redirect('Login_Controller', 'refresh');
   }
}//END CLASS LOGIN CONTROLLER