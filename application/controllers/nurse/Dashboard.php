<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {


	//This function load the view for the welcome page i.e the first page when you enter the URL
	public function index()
	{
		
		
		$this->template->load('nurse', 'default','homeview');
	}

//this function diaplya list of diagnosed patient for the nurse 
	public function managePatient()
	{

		$data['viewpatient'] = $this->NurseModel->viewPatient();
		$this->template->load('nurse', 'default','managepatient', $data);
	}
	//this function selects a single patient for nurse to administer treatment
	public function administerPrescription(){

		$patient_id 		= $this->uri->segment(4); //this values came from the URI
		$prescription_date  = $this->uri->segment(5); 
		
		$data['viewpatient'] = $this->NurseModel->nurseViewForTreatment($patient_id, $prescription_date);
		$this->template->load('nurse', 'default','administerprescription', $data);
	}

	//this function submits the medication offred by nurse to the patient_health_record
	public function nurseSubmitRecord(){

		//setting validation rules
		$this->form_validation->set_rules('patient_id', 'patient id','trim|required');
		$this->form_validation->set_rules('diagnosis', 'diagnosis', 'trim|required');
		$this->form_validation->set_rules('prescription_date', 'prescription date', 'trim|required');
		$this->form_validation->set_rules('injection_update', 'injection_update', 'trim|required');

		$id = $this->input->post('patient_id');
		$diag_date = $this->input->post('prescription_date');
		if ($this->form_validation->run() == FALSE)
          {
	 		
			redirect(base_url() . "nurse/Dashboard/administerPrescription/". $id ."/" . $diag_date, 'refresh');
			


		  }else{

             

			//else is validation is true then create post array
            $injection_update          		= $this->input->post('injection_update');
            $patient_id          			= $this->input->post('patient_id');
            $prescription_date				= $this->input->post('prescription_date');
            $nurse_id						= $this->session->userdata('pfNo');
            //data array
            $data['injection_update']  		= $injection_update;
            $data['nurse_id']  		   		= $nurse_id;

			$this->NurseModel->NurseInjectionUpdate($data, $patient_id, $prescription_date);

			//this table add pharmacy prescription to the patient health history table
			//History table input
			$history['injection_update']  = $injection_update;
			$history['nurse_id']  		  = $nurse_id;

			$this->NurseModel->HistoryInjectionUpdate($history, $patient_id, $prescription_date);

			//Activities log  array
    	$data = array(
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Treated this patient with ID ' ." " . $patient_id . " ".'as requested by the Doctor'
         ); 
    	//insert Activities
    	$this->NurseModel->addLog($data);

    	
		$this->session->set_flashdata('success', 'Patient Succesfully Attended');

		//redirect to the Nurse to attend to other patients
		redirect(base_url() . "nurse/Dashboard/managePatient", 'refresh');
		//redirect('doctor/Dashboard/forwardPatient/'. $patient_id, 'refresh');

		}	

	}

	//this function helps the nurse to not see any patient whose treatment is over when Done is clicked
	public function nurseAdministerComplete(){
		$patient_id 		= $this->uri->segment(4); //this values came from the URI
		$prescription_date  = $this->uri->segment(5); 
		$data['nurse_done'] = 1;
		
		$this->NurseModel->nurseAdministerComplete($data, $patient_id, $prescription_date);
		
		$this->session->set_flashdata('success', 'Patient Treatment Dosage Completed Succesfully');
		$this->session->set_flashdata('error', 'Patient Have been removed from Queue');


		//Activities log  array
    	$data = array(
        'pfNo'            =>  $this->session->userdata('pfNo'),
        'email'           =>  $this->session->userdata('email'),
        'task_time'       =>  date("Y-m-d H:i:s"),
        'task'            =>  'Treated this patient with ID ' ." " . $patient_id . " ".'Treatment Have Been Completed'
         ); 
    	//insert Activities
    	$this->NurseModel->addLog($data);

		//redirect to the Nurse to attend to other patients
		redirect(base_url() . "nurse/Dashboard/managePatient", 'refresh');
	}
}




